<div class="navbar2">
	<div class="container-fluid top-menu max-width"> 
		<ul class="unstyled inline ul_navbar2">
		    <li><a href="{{ URL::route('v1_news_items_front') }}"><i class="fa fa-newspaper-o"></i>&nbsp;News</a></li> 
		    <li><a href="{{ URL::route('v1_user_acts_parliament') }}"><i class="fa fa-book"></i>&nbsp;Laws &amp; Regulations</a></li>
		    <li><a href="{{ URL::route('v1_tax_treaties_ratified') }}"><i class="fa fa-file-text-o"></i>&nbsp; Tax Treaties</a></li>
		    <li><a href="{{ URL::route('v1_cases_court_appeal') }}"><i class="fa fa-legal">&nbsp;</i>Cases</a></li>
		    <li><a href="{{ URL::route('v1_pn_addons') }}"><i class="fa fa-plus-square"></i>&nbsp;Resources</a></li>
		</ul>
	</div>
</div> 
