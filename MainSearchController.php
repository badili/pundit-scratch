<?php
/**
 * The main search controller.
 *
 * @copyright (badili)
 * @version v2
 * @author Gideon K. <gideon.kamau@badili.co.ke>
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Dashboard\SettingsController;
use App\Models\Act;
use App\Models\ActDivision;
use App\Models\ActPart;
use App\Models\ActSection;
use App\Models\ActVersion;
use App\Models\Bookmark;
use App\Models\CaseJudge;
use App\Models\CourtCase;
use App\Models\SettingsVariables;
use App\Models\TaxTreaty;
use App\Models\TaxTreatyArticle;
use App\Models\TaxTreatySection;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Jenssegers\Optimus\Optimus, App\Http\Controllers\Controller;
use Response;

use Laravel\Scout\ScoutServiceProvider;
use TeamTNT\TNTSearch\TNTSearch;

/**
 * Main search controller class for all content types.
 */
class MainSearchController extends Controller {
    /**
     * Set the constants to use for this class.
     */
    public function __construct() {
        $this->relevance = 3;
        $this->cache_duration = config('pundit.cache_duration');
        $this->search_pagination = config('app.search_items_per_page');

        // lets define the search options here
        $this->boolean_search = [
          'storage'  => storage_path(), //place where the index files will be stored
          'fuzziness' => false,
          'searchBoolean' => true,
          'asYouType' => false,
        ];

        $this->fuzzy_search = [
          'storage'  => storage_path(), //place where the index files will be stored
          'fuzziness' => true,
          'fuzzy' => [
              'prefix_length' => 2,
              'max_expansions' => 50,
              'distance' => 2
          ],
          'asYouType' => false,
          'searchBoolean' => false,
        ];
    }

    /**
     * Search the Courtcases and return the results as an organized associative array
     * 
     * @param  \Illuminate\Http\Request $request
     * @deprecated v1 Initial search using the Searchy package.
     * @return \Illuminate\View\View
     */
    public function searchCases(Request $request) {
        $optimus = Controller::optimus();
        $search_query = $request->input('search_query');
        $party = $request->input('party');
        $judge = $request->input('judge');
        $is_advanced = $request->input('is_advanced');
        $citation = $request->input('citation');
        $paginate_no = 5;

        if (preg_match('/[^a-zA-Z\(\)\" ,.]+/',$search_query, $matches) || preg_match('/[^a-zA-Z\(\)\" ,.]+/',$defendant, $matches)) {
          // The serach query has chars drop them like they're hot!
          return redirect()->back()->with('alert_danger','Please, remove any special characters and try searching again!');
        }

        if ($request->isMethod('get')) {
          if ($is_advanced == True) {
            // Do advanced filtering here
            $cases_search = \Searchy::search('court_cases as ccs')
              ->fields('ccs.delivery_details','ccs.key_terms','ccs.summary', 'ccj.judgement')
              ->query($search_query)
              ->select('ccs.*')
              ->getQuery()
              ->join('cases_judges as ccj', 'ccs.id', '=', 'ccj.cases_id');

            // add the party if specified
            if($party != ''){
              $cases_search->where('plaintiff', 'like', "%{$party}%")
                ->where('defendant', 'like', "%{$party}%");
            }

            $cases_search->where('is_published', 1)
              ->get()
              ->toArray();

            $cases = CourtCase::hydrate($cases_search);
          } else {
            $cases = CourtCase::hydrate(\Searchy::court_cases('delivery_details', 'plaintiff', 'defendant', 'key_terms', 'signal', 'summary')
              ->query($search_query)
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->where('is_published', 1)
              ->toArray()
            );
          }
          return view('pages/case-search-results_blade', compact('cases', 'paginate_no', 'optimus', 'search_query'));
        }
    }
    /**
     * Remove special chars from the search query input.
     * 
     * @param  String $string search query input.
     * @return String The string with no special chars.
     */
    public function clean($string) {
       return preg_replace('/[^A-Za-z0-9]/', ' ', $string); // Removes special chars.
    }

    /**
     * Determines the search criteria to use.
     * 
     * When the search query is enclosed with quotes, the boolean search method is used else the fuzzy search is used
     * 
     * @param  string $search_query The search query as specified by the user
     * @return array                Returns an array with the search query to use as well as the search parameters
     */
    private function determine_search_type($search_query){
      // check if we need to perform a fuzzy search or a boolean search
      preg_match('/^"/', $search_query, $matches);
      // var_dump($matches);
      if(preg_match('/^"/', $search_query, $matches) == 0){
        $search_settings = $this->fuzzy_search;
        $new_query = $search_query;
        $search_type ='fuzzy search';
      }
      else{
        $search_settings = $this->boolean_search;
        $new_query = preg_replace('/[\"]/', '', $search_query);
        $search_type = 'boolean search';
      }
      // dump($search_type);
      // $search_settings['original'] = $search_query;
      return ['search_type' => $search_type, 'search_settings' => $search_settings, 'search_query' => $new_query];
    }

    /**
     * Use Scout to search for the already indexed act sections in the DB
     * 
     * The search uses the act_sections index generated and returns the reaults as
     * a list of hits, as an organized associative array with act->version->part->section order.
     * When filter parameters such as version year are provided the filter process
     * follows the searching. After the filtering another filter mapping to remove the null items is perfomed.
     * 
     * @param  \Illuminate\Http\Request laravel request object 
     * @param  \TeamTNT\TNTSearch\TNTSearch laravel scout tntsearch driver object
     * @return \Illuminate\View\View the search results page.
     */
    public function searchActsV2(Request $request, TNTSearch $tnt) {
        $optimus = Controller::optimus();

        $search_query = $request->input('search_query');
        $year_start = $request->input('version_year_start');
        $year_stop = $request->input('version_year_end');
        $selected_act_title_id = $request->input('selected_act_title');
        $advanced_act_title_keywords = $request->input('act_title');
        $is_advanced = $request->input('is_advanced');
        $search_type = $request->input('search_type');
        $title = $request->input('title');
        $page_no = ($request->input('page') ? $request->input('page') : 1);
        $section_type = ($request->input('type_of_content') ? $request->input('type_of_content') : []);
        $paginate_no = 20;

        // search from the TNT index
        $tnt1 = new TNTSearch();
        $tnt1->loadConfig([
            'driver'    => Config::get('database.connections.mysql.driver'),
            'host'      => Config::get('database.connections.mysql.host'),
            'database'  => Config::get('database.connections.mysql.database'),
            'username'  => Config::get('database.connections.mysql.username'),
            'password'  => Config::get('database.connections.mysql.password'),
            'storage'   => storage_path(),
            'stemmer'   => TeamTNT\TNTSearch\Stemmer\PorterStemmer::class
        ]);
        $tnt1->selectIndex("act_sections.index");
        
        if($search_type == 'full_text'){      // exact matches
            $tnt1->fuzziness(false);
            $res = $tnt1->searchBoolean($search_query);

            // we dont have the docScores
            $fuzzy_ids = $res['ids'];
        }
        else{
            $tnt1->fuzziness(true);
            $res = $tnt1->search($search_query);
            $fuzzy_ids = array_keys($res['docScores']);      // used when $tnt1->search is called
            // $fuzzy_ids = $res['ids'];
        }

        // lets explore these results
        $all_sections1 = ActSection::whereIn('act_sections.id', $fuzzy_ids);
        $isPartsJoined = False;
        $isVersionsJoined = False;

        //lets apply the filters if any
        // formulate the constraints
        if(count($section_type) > 0 && !in_array('all', $section_type)){       // just to take care of a glitch on the UX
            $all_sections1->join('act_parts', 'act_sections.act_part_id', '=', 'act_parts.id');
            $isPartsJoined = True;
            if(in_array('schedules', $section_type)){
                $all_sections1->where('act_parts.doc_type', 'act_schedule');
            }
            if(in_array('subsidiaries', $section_type)){
                $all_sections1->where('act_parts.doc_type', 'act_subsidiary');
            }
            if(in_array('sections', $section_type)){
                $all_sections1->where('act_parts.doc_type', 'act_part');
            }
        }

        if($year_start != '' || $year_stop != ''){
            if(!$isPartsJoined){
                $all_sections1->join('act_parts', 'act_sections.act_part_id', '=', 'act_parts.id');
            }
            $all_sections1->join('act_versions', 'act_parts.act_version_id', '=', 'act_versions.id');
            $isVersionsJoined = True;
            
            if($year_start != ''){
                $all_sections1->where('act_versions.version_year', '>=', $year_start);
            }
            if($year_stop != ''){
                $all_sections1->where('act_versions.version_year', '<=', $year_stop);
            }
        }

        if($title != ''){
            if(!$isPartsJoined){
                $all_sections1->join('act_parts', 'act_sections.act_part_id', '=', 'act_parts.id');
                $isPartsJoined = True;
            }
            if(!$isVersionsJoined){
                $all_sections1->join('act_versions', 'act_parts.act_version_id', '=', 'act_versions.id');
                $isVersionsJoined = True;
            }
            $all_sections1->join('acts', 'act_versions.act_id', '=', 'acts.id');
            $all_sections1->where('acts.act_title', 'like', "%$title%");
        }

        $filtered_ids = $all_sections1->pluck('act_sections.id')->toArray();
        // from the list of filtered section ids, lets re-order them based on the initial results from the TNT index
        $filtered_ordered_ids = array_intersect($fuzzy_ids, $filtered_ids);

        // paginate and get the items to display according to the current page
        list($paginated, $pageItems) = $this->search_paginate($filtered_ordered_ids, $page_no, $paginate_no, $request->path());
        $total_hits = count($filtered_ordered_ids);

        $highlight_options = [
            'tagOptions' => [
                'class' => 'search_results'
            ]
        ];
        $ordered = array_map(function($id) use($all_sections1) {
            return ActSection::where('act_sections.id', $id)->first();
        }, $pageItems);

        $final_results = $this->format_results($sections=$ordered);
        return view('v2/pages/search_v2', compact('year_start','year_stop','section_type', 'search_type', 'title', 'optimus', 'search_query', 'paginated', 'final_results', 'total_hits', 'tnt1', 'highlight_options'));
    }

    private function search_paginate($items, $currentPage, $perPage, $path){
        if(is_null($perPage) || $perPage == 0){
            // if the settings cant be read correctly, force the number of serch results
            $perPage = 15;
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        if ($currentPage == 1) {
            $start = 1;
        }
        else {
            $start = ($currentPage - 1) * $perPage;
        }
        // Slice the collection to get the items to display in current page
        $currentPageItems = array_slice($items, $start-1, $perPage, true);

        // Create our paginator and pass it to the view
        $sections_count = count($items);
        return array(new LengthAwarePaginator($currentPageItems, $sections_count, $perPage, $currentPage, ['path' => $path]), $currentPageItems);
    }

    /**
     * Given a laravel collection of act sections and the num items to paginate
     * then create a new lenth aware paginaotor and return the items.
     * 
     * @param  Collection $sections Laravel collection
     * @param  int $perPage  Number of items to return for acts search
     * @return \Illuminate\Pagination\LengthAwarePaginator     
     */
    private function manual_paginate($sections, $perPage){
      // Get current page form url e.g. &page=1
      // remove the nulls and create a laravel collection from the resultant array
      $sections = collect($sections->filter()->all());
      if(is_null($perPage) || $perPage == 0){
        // if the settings cant be read correctly, force the number of serch results
        $perPage = 20;
      }
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      if ($currentPage == 1) {
        $start = 1;
      }
      else {
        $start = ($currentPage - 1) * $perPage;
      }
      // Slice the collection to get the items to display in current page
      $currentPageItems = $sections->slice($start-1, $perPage)->all();

      // Create our paginator and pass it to the view
      $sections_count = count($sections);
      return new LengthAwarePaginator($currentPageItems, $sections_count, $perPage);
    }

    /**
     * Format the act sections results from scout search and return them organized hiereachicaly,
     * 
     * @param  \Illuminate\Database\Eloquent\Collection  $sections Act sections with hits in results
     * @return Array  organized act results based on the search hits.
     */
    public function format_results($sections=[]) {
        $all_results['sections'] = $sections;
        $final_results = [];
        $overall_results_count = 0;
        foreach($all_results as $act_key => $act_key_databag) {
            foreach ($act_key_databag as $key => $actual_data) {
                if (!empty($actual_data)) {
                    $section = ActSection::with('division')->where('id', '=', $actual_data->id)->first();
                    $division = $section->division()->first();

                    if ( $division == null) {
                        // Some sections dont belong to divisions they belong to a part
                        $part = ActPart::with('sections')->where('id','=',$section->act_part_id)->first();
                    }
                    else {
                        $division_name = $division->title;
                        $division_id = $division->id;
                        $part = ActPart::with('version')->where('id','=',$division->act_part_id)->first();
                    }

                    $act_title = $part->version()->first()->act()->first()->act_title;
                    $part_no = $part->no;
                    $part_name = $part->title;
                    $version_year = $part->version()->first()->version_year;

                    // lets add the results to the final array
                    // define the act array if its not there
                    if( !isset($final_results[$act_title]) ){
                        $final_results[$act_title] = [
                            'parts' => []
                        ];
                    }

                    // define the act parts if not there
                    if( !isset($final_results[$act_title]['parts'][$section->act_part_id]) ){
                        $final_results[$act_title]['parts'][$section->act_part_id] = [
                            'no'        => $part_no,
                            'name'      => $part_name,
                            'year'      => $version_year,
                            'sections'  => []
                        ];
                    }
                    
                    if ( $division != null) {
                        if( !isset($final_results[$act_title]['parts'][$section->act_part_id]['divisions']) ){
                            $final_results[$act_title]['parts'][$section->act_part_id]['divisions'] = [];
                        }
                        if( !isset($final_results[$act_title]['parts'][$section->act_part_id]['divisions'][$division_id]) ){
                            $final_results[$act_title]['parts'][$section->act_part_id]['divisions'][$division_id] = [
                                'name'      => $division_name,
                                'sections'  => []
                            ];
                        }
                    }

                    if ( $division != null) {
                        array_push($final_results[$act_title]['parts'][$section->act_part_id]['divisions'][$division_id]['sections'], $actual_data);
                    }
                    else{
                        array_push($final_results[$act_title]['parts'][$section->act_part_id]['sections'], $actual_data);
                    }
                }
            } // End if
        }
        return $final_results;
    }

    /**
     * Use Scout to search for the already indexed treaty sections in the DB
     * 
     * The search uses the treaty_sections index generated and returns the reaults as
     * a list of hits, as an organized associative array with treaty->part->section order.
     * When filter parameters such as signing year are provided the filter process
     * follows the searching. After the filtering another filter mapping to remove the null items is perfomed.
     * 
     * @param  \Illuminate\Http\Request   $request 
     * @param  \TeamTNT\TNTSearch\TNTSearch $tnt     
     * @return \Illuminate\View\View the search results page.
     */
    public function searchTreatyV2(Request $request, TNTSearch $tnt) {
      $optimus = Controller::optimus();

      $settings = new SettingsController;
      $countries = $settings->getSettingsVariable('countries');
      $treaties_status = $settings->getSettingsVariable('treaty_status');

      $search_query = $request->input('search_query');
      $search_type = $request->input('search_type');
      $partner2 = $request->input('partner_country');
      $year_signed = $request->input('year_signed');
      $treaty_status_input = $request->input('treaty_status');
      $type_of_search = $request->input('type_of_search');
      $is_advanced = $request->input('is_advanced');
      $year_start = $request->input('version_year_start');
      $year_stop = $request->input('version_year_end');
      $page_no = ($request->input('page') ? $request->input('page') : 1);
      $paginate_no = 20;

        // search from the TNT index
        $tnt1 = new TNTSearch();
        $tnt1->loadConfig([
            'driver'    => Config::get('database.connections.mysql.driver'),
            'host'      => Config::get('database.connections.mysql.host'),
            'database'  => Config::get('database.connections.mysql.database'),
            'username'  => Config::get('database.connections.mysql.username'),
            'password'  => Config::get('database.connections.mysql.password'),
            'storage'   => storage_path(),
            'stemmer'   => TeamTNT\TNTSearch\Stemmer\PorterStemmer::class
        ]);
        $tnt1->selectIndex("tax_treaties_sections.index");
        
        if($search_type == 'full_text'){      // exact matches
            $tnt1->fuzziness(false);
            $res = $tnt1->searchBoolean($search_query);

            // we dont have the docScores
            $fuzzy_ids = $res['ids'];
        }
        else{
            $tnt1->fuzziness(true);
            $res = $tnt1->search($search_query);
            $fuzzy_ids = array_keys($res['docScores']);      // used when $tnt1->search is called
            // $fuzzy_ids = $res['ids'];
        }

        // lets explore these results
        $treaty_sections = DB::table('tax_treaties_sections as tts')->join('tax_treaties_articles as tta', 'tts.tax_treaty_article_id', '=', 'tta.id')->join('tax_treaties as tt', 'tta.tax_treaty_id', '=', 'tt.id');
        $treaty_sections->whereIn('tts.id', $fuzzy_ids);

        // get the treaty status if need be
        if($treaty_status_input != ''){
            $treaty_status_id = SettingsVariables::where('setting_key', 'treaty_status')->where('variable_key', $treaty_status_input)->pluck('id')->toArray()[0];
            $treaty_sections->where('tt.publish_status_id', $treaty_status_id);
        }

        if($partner2 != '' && $partner2 != 'all'){
            $treaty_sections->where('tt.partner2_id', $partner2);
        }

        if($year_start != '' || $year_stop != ''){
            if($year_start != ''){
                $treaty_sections->where('tt.signing_date', '>=', $year_start);
            }
            if($year_stop != ''){
                $treaty_sections->where('tt.signing_date', '<=', $year_stop);
            }
        }
        $filtered_ids = $treaty_sections->pluck('tts.id')->toArray();
        // from the list of filtered section ids, lets re-order them based on the initial results from the TNT index
        $filtered_ordered_ids = array_intersect($fuzzy_ids, $filtered_ids);

        // paginate and get the items to display according to the current page
        list($paginated, $pageItems) = $this->search_paginate($filtered_ordered_ids, $page_no, $paginate_no, $request->path());
        $total_hits = count($filtered_ordered_ids);

        $highlight_options = [
            'tagOptions' => [
                'class' => 'search_results'
            ]
        ];
        $ordered = array_map(function($id) use($treaty_sections) {
            return TaxTreatySection::where('id', $id)->first();
        }, $pageItems);

        $final_results = $this->format_treaty_results($tax_treaties_sections=$ordered);

        return view('v2/pages/search_treaties_v2', compact('treaty_status_input','partner2','year_stop','year_start','final_results', 'treaties_status', 'paginated', 'countries','tax_treaties_sections', 'optimus', 'search_query', 'search_type', 'total_hits', 'tnt1', 'highlight_options'));
    }

    /**
     * Format the tax treaty sections to return to the frontend.
     * 
     * @param  \Illuminate\Database\Eloquent\Collection $tax_treaties_sections.
     * @return Array  organized act results based on the search hits.
     */
    private function format_treaty_results($tax_treaties_sections=null) {
        $all_results['tax_treaties_sections'] = $tax_treaties_sections;
        $final_results = [];

        foreach ($tax_treaties_sections as $treaty_section) {
            $parent_article = $treaty_section->section_article()->first();
            $parent_article_name = $treaty_section->section_article()->first()->article_name;
            $treaty_name = $parent_article->tax_treaty()->first()->treaty_name;

            if (!array_key_exists($treaty_name, $final_results)) {
                $final_results[$treaty_name] = [];
            }
            
            if (!array_key_exists($parent_article_name, $final_results[$treaty_name])) {
                $final_results[$treaty_name][$parent_article_name] = [];
            }

            // push our section
            array_push($final_results[$treaty_name][$parent_article_name], $treaty_section);
        }
        return $final_results;
    }

    /**
     * Use Scout to search for the already indexed court cases in the DB
     * 
     * The search uses the case_judges index generated and returns the reaults as
     * a list of hits, as an organized associative array with judgement->courtcase order.
     * When filter parameters such as judgement year are provided the filter process
     * follows the searching. After the filtering another filter mapping to remove the null items is perfomed.
     * 
     * @param  \Illuminate\Http\Request   $request
     * @param  \TeamTNT\TNTSearch\TNTSearch $tnt.
     * @return \Illuminate\View\View The search cases page.
     */
    public function searchCasesV2(Request $request, TNTSearch $tnt) {
        $optimus = Controller::optimus();

        $search_query = $request->input('search_query');
        $search_type = $request->input('search_type');
        $party = $request->input('party');
        $judge = $request->input('judge');
        $is_advanced = $request->input('is_advanced');
        $citation = $request->input('citation');
        $year_start = $request->input('version_year_start');
        $year_stop = $request->input('version_year_end');
        $court_input = $request->input('court_input');

        $page_no = ($request->input('page') ? $request->input('page') : 1);
        $paginate_no = 20;

        // search from the TNT index
        $tnt1 = new TNTSearch();
        $tnt1->loadConfig([
            'driver'    => Config::get('database.connections.mysql.driver'),
            'host'      => Config::get('database.connections.mysql.host'),
            'database'  => Config::get('database.connections.mysql.database'),
            'username'  => Config::get('database.connections.mysql.username'),
            'password'  => Config::get('database.connections.mysql.password'),
            'storage'   => storage_path(),
            'stemmer'   => TeamTNT\TNTSearch\Stemmer\PorterStemmer::class
        ]);
        $tnt1->selectIndex("case_judges.index");
        
        if($search_type == 'full_text'){      // exact matches
            $tnt1->fuzziness(false);
            $res = $tnt1->searchBoolean($search_query);

            // we dont have the docScores
            $fuzzy_ids = $res['ids'];
        }
        else{
            $tnt1->fuzziness(true);
            $res = $tnt1->search($search_query);
            $fuzzy_ids = array_keys($res['docScores']);      // used when $tnt1->search is called
            // $fuzzy_ids = $res['ids'];
        }

        // exploring the results
        $court_cases = DB::table('cases_judges as cjs');

        if($party != ''){
            $court_cases->join('court_cases', 'cjs.cases_id', '=', 'ccs.id')->where('plaintiff', 'like', "%$party%")->orWhere('defendant', 'like', "%$party%");
        }
        if($judge != ''){
            $court_cases->join('judges', 'cjs.judges_id', '=', 'judges.id')->where('judge_name', 'like', "%$judge%");
        }
        $filtered_ids = $court_cases->pluck('cjs.id')->toArray();

        // from the list of filtered section ids, lets re-order them based on the initial results from the TNT index
        $filtered_ordered_ids = array_intersect($fuzzy_ids, $filtered_ids);

        // paginate and get the items to display according to the current page
        list($paginated, $pageItems) = $this->search_paginate($filtered_ordered_ids, $page_no, $paginate_no, $request->path());
        $total_hits = count($filtered_ordered_ids);

        $highlight_options = [
            'tagOptions' => [
                'class' => 'search_results'
            ]
        ];
        $final_results = array_map(function($id) use($court_cases) {
            return CaseJudge::where('id', $id)->first();
        }, $pageItems);

        return view('v2/pages/search_cases_v2', compact('court_input','year_start', 'year_stop', 'final_results', 'optimus', 'search_query', 'search_type', 'paginated', 'total_hits', 'tnt1', 'highlight_options'));
    }

    /**
     * Old Search the Acts in the database using Searchy.
     *
     * @deprecated v1 Initial search Acts method using the Searchy provider.
     * 
     * @param  \Illuminate\Http\Request $request     
     * @param  boolean $is_advanced 
     * @return                
     */
    public function searchActs(Request $request, $is_advanced=False) {
      $advanced_act_title_keywords = $request->input('act_title');
      $search_query = $request->input('search_query');
      $is_advanced = $request->input('is_advanced');
      $type_of_search = $request->input('type_of_search');
      $paginate_no = 5;
      $all_results =[];

      if (preg_match('/[^a-zA-Z\(\)\" ,.]+/',$search_query, $matches) || preg_match('/[^a-zA-Z\(\)\" ,.]+/',$advanced_act_title_keywords, $matches)) {
        // The search query has chars drop them like they're hot!
        return redirect()->back()->with('alert_danger','Please, remove any special characters and try searching again!');
      }

      $optimus = Controller::optimus();
      if ($request->isMethod('get')) {
        if ($is_advanced == True) {
          // Do the advanced search here
          $year_start = $request->input('version_year_start');
          $year_stop = $request->input('version_year_end');
          $type_of_content = $request->input('type_of_content');
          $acts = Act::hydrate(\Searchy::acts('act_title')->query($advanced_act_title_keywords)->get()->toArray());
          $content_types_length = count($type_of_content);

          if (in_array("all", $type_of_content)) {
            // Search for every type of section
            if (!empty($year_start) && !empty($year_stop)) {
              // We have a title year_start and end then go on and filter
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->join('act_parts as ap', 'acs.act_part_id', '=', 'ap.id')
              ->join('act_versions as avs','ap.act_version_id', '=', 'avs.id')
              ->where('avs.version_year', '>=', $year_start)
              ->where('avs.version_year', '<=', $year_stop)
              ->join('acts as acc', 'avs.act_id', '=', 'acc.id')
              ->where('acc.act_title', 'like', '%'.$advanced_act_title_keywords.'%')
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray()); 
            }
            else if (!empty($year_start)) {
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->join('act_parts as ap', 'acs.act_part_id', '=', 'ap.id')
              ->join('act_versions as avs','ap.act_version_id', '=', 'avs.id')
              ->where('avs.version_year', '>=', $year_start)
              ->join('acts as acc', 'avs.act_id', '=', 'acc.id')
              ->where('acc.act_title', 'like', '%'.$advanced_act_title_keywords.'%')
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray());
            }
            else {
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->orderBY('relevance', 'DESC')
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray());
            }
          }
          else {
            // Search the various included types
            if (!empty($year_start) && !empty($year_stop)) {
              // We have a title year_start and end then go on and filter
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->orderBY('relevance', 'DESC')
              ->join('act_parts as ap', 'acs.act_part_id', '=', 'ap.id')
              ->wherein('ap.doc_type', $type_of_content)
              ->join('act_versions as avs','ap.act_version_id', '=', 'avs.id')
              ->where('avs.version_year', '>=', $year_start)
              ->where('avs.version_year', '<=', $year_stop)
              ->join('acts as acc', 'avs.act_id', '=', 'acc.id')
              ->where('acc.act_title', 'like', '%'.$advanced_act_title_keywords.'%')
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray()); 
            }
            else if (!empty($year_start)) {
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->orderBY('relevance', 'DESC')
              ->join('act_parts as ap', 'acs.act_part_id', '=', 'ap.id')
              ->wherein('ap.doc_type', $type_of_content)
              ->join('act_versions as avs','ap.act_version_id', '=', 'avs.id')
              ->where('avs.version_year', '>=', $year_start)
              ->join('acts as acc', 'avs.act_id', '=', 'acc.id')
              ->where('acc.act_title', 'like', '%'.$advanced_act_title_keywords.'%')
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray());
            }
            else {
              $sections = ActSection::hydrate(\Searchy::search('act_sections as acs')->fields('acs.title', 'acs.content', 'acs.marginal_notes')
              ->query($search_query)
              ->select('acs.*')
              ->getQuery()
              ->orderBY('relevance', 'DESC')
              ->join('act_parts as ap', 'acs.act_part_id', '=', 'ap.id')
              ->wherein('ap.doc_type', $type_of_content)
              ->having('relevance', '>', $this->relevance)
              ->get()
              ->toArray());
            }
          }
        } 
        else {
          // Search everything , this is costly
          $acts = Act::hydrate(\Searchy::acts('act_title')->query($search_query)->get()->toArray());
          $versions = ActVersion::hydrate(\Searchy::act_versions('preamble', 'side_notes')->query($search_query)->getQuery()
            ->orderBY('relevance', 'DESC')
            ->where('is_published', 1)
            ->having('relevance', '>', $this->relevance)
            ->distinct('act_id')
            ->get()->toArray());
          $parts = ActPart::hydrate(\Searchy::act_parts('title')->query($search_query)->getQuery()
            ->having('relevance', '>', $this->relevance)
            ->orderBY('relevance', 'DESC')
            ->get()->toArray());
          $divisions = ActDivision::hydrate(\Searchy::act_divisions('title')->query($search_query)->getQuery()
            ->having('relevance', '>', $this->relevance)
            ->orderBY('relevance', 'DESC')
            ->get()->toArray());
          $sections = ActSection::hydrate(\Searchy::act_sections('title', 'content', 'marginal_notes')->query($search_query)->getQuery()
            ->having('relevance', '>', $this->relevance)
            ->orderBY('relevance', 'DESC')
            ->get()->toArray());
          $act_versions = [];
          $act_ids = [];
          
          foreach ($versions as $key => $value) {
            $act_id = $value['act_id'];
            if (!in_array($act_id, $act_ids)) {
              array_push($act_ids, $act_id);
              array_push($act_versions, $value);
            }
          }

          $all_results['versions'] = $act_versions;
          $all_results['parts'] = $parts;
          $all_results['divisions'] = $divisions;
        }
        $all_results['acts'] = $acts;
        $all_results['sections'] = $sections;
        // Build the final array for frontend presentation
        $final_results = [];
        $overall_results_count = 0;
        foreach($all_results as $act_key => $act_key_databag) {
            foreach ($act_key_databag as $inner_key => $actual_data) {
              if ($act_key == 'acts') {
                $act_title = $actual_data->act_title;
                if (!array_key_exists($act_title, $final_results)) {
                  $final_results[$act_title] = [];
                  array_push($final_results[$act_title], $actual_data);
                  $overall_results_count += 1;
                  }
                else {
                  array_push($final_results[$act_title], $actual_data);
                  $overall_results_count += 1;
                }
              }
              else if ($act_key == 'versions') {
                $act_title = $actual_data->act()->first()->act_title;
                if (!array_key_exists($act_title, $final_results)) {
                  $final_results[$act_title] = [];
                  array_push($final_results[$act_title], $actual_data);
                  $overall_results_count += 1;
                  }
                else {
                  array_push($final_results[$act_title], $actual_data);
                  $overall_results_count += 1;
                }
              }
              else if ($act_key == 'parts') {
                $part_name = $actual_data->title;
                $act_title = $actual_data->version()->first()->act()->first()->act_title;

                if (!array_key_exists($act_title, $final_results)) {
                  $final_results[$act_title] = [];
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                      $final_results[$act_title][$part_name] = [];
                      array_push($final_results[$act_title][$part_name], $actual_data);
                      $overall_results_count += 1;
                  }// second level part name exisiting 
                  else {
                    array_push($final_results[$act_title][$part_name], $actual_data);
                    $overall_results_count += 1;
                  }
                }
                else {
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                      $final_results[$act_title][$part_name] = [];
                      array_push($final_results[$act_title][$part_name], $actual_data);
                      $overall_results_count += 1;
                  }// second level part name exisiting 
                  else {
                    array_push($final_results[$act_title][$part_name], $actual_data);
                    $overall_results_count += 1;
                  }
                }
              }
              else if ($act_key == 'divisions') {
                $division_name = $actual_data->title;
                $part = ActPart::with('version')->where('id','=',$actual_data->act_part_id)->first();
                $part_name = $part->title;
                $act_title = $part->version()->first()->act()->first()->act_title;

                if (!array_key_exists($act_title, $final_results)) {
                  $final_results[$act_title] = [];
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                    $final_results[$act_title][$part_name] = [];
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }// second level part name exisiting 
                  else {
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
                else {
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                      $final_results[$act_title][$part_name] = [];
                      if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                        $final_results[$act_title][$part_name][$division_name] = [];
                        array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                        $overall_results_count += 1;
                      } // third level division existing
                      else {
                        array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                        $overall_results_count += 1;
                      }
                  }// second level part name exisiting 
                  else {
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                } 
              }
              else if ($act_key == 'sections') {
                $section = ActSection::with('division')->where('id', '=', $actual_data->id)->first();
                $division = $section->division()->first();
                if (!count($division) > 0) {
                  // Some sections dont belong to divisions they have direct relationship with a part
                  $part = ActPart::with('sections')->where('id','=',$section->act_part_id)->first();
                  $part_name = $part->title;
                  $act_title = $part->version()->first()->act()->first()->act_title;
                  $division_name = 'direct_sections';
                } else {
                  $division_name = $division->title;
                  $part = ActPart::with('version')->where('id','=',$division->act_part_id)->first();
                  $part_name = $part->title;
                  $act_title = $part->version()->first()->act()->first()->act_title;
                }
                if (!array_key_exists($act_title, $final_results)) {
                  $final_results[$act_title] = [];
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                    $final_results[$act_title][$part_name] = [];
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }// second level part name exisiting 
                  else {
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
                else {
                  if(!array_key_exists($part_name, $final_results[$act_title])) {
                      $final_results[$act_title][$part_name] = [];
                      if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                        $final_results[$act_title][$part_name][$division_name] = [];
                        array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                        $overall_results_count += 1;
                      } // third level division existing
                      else {
                        array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                        $overall_results_count += 1;
                      }
                  }// second level part name exisiting 
                  else {
                    if(!array_key_exists($division_name, $final_results[$act_title][$part_name])) {
                      $final_results[$act_title][$part_name][$division_name] = [];
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    } // third level division existing
                    else {
                      array_push($final_results[$act_title][$part_name][$division_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                } // exists check
            }// act key
          }// inner foreach
        } // outer foreach
        // $final_results = new LengthAwarePaginator($final_results, $overall_results_count, 10);
        // $final_results = $final_results->setPath($request->url());

        if($type_of_search == 'quick_view') {
          return view('pages/acts-search-simple-view', compact('advanced_act_title_keywords','final_results', 'overall_results_count', 'paginate_no', 'optimus', 'content', 'search_query'));
        }
        else {
          return view('pages/acts-search', compact('advanced_act_title_keywords','final_results', 'overall_results_count', 'paginate_no', 'optimus', 'content', 'search_query'));
        }
      }
    }
    /**
     * Get all the employment and labour courts and return.
     * 
     * @return \Illuminate\Database\Eloquent\Collection 
     */
    private function get_act(){ 
        // get the court of high_appeal_tribunl from setting variable
        $s_controller = new SettingsController();
        $emp_labor = $s_controller->getSettingsVariable('court_types','emp_labor_court');
        $labour_employment = DB::table('settings_variables')->where('sub_setting_key', 'emp_labor_court')->select('id as court_id')->get();
        return $labour_employment;
    }
    /**
     * Old Search Treaties using the Searchy package,
     *
     * @deprecated v1 Initial search Treaties method using the Searchy provider.
     * @param  \Illuminate\Http\Request $request 
     */
    public function SearchTreaties(Request $request) {
      $optimus = Controller::optimus();
      $settings = new SettingsController;
      $countries = $settings->getSettingsVariable('countries');
      $treaties_status = $settings->getSettingsVariable('treaty_status');
      $search_query = $request->input('search_query');
      if (preg_match('/[^a-zA-Z\(\)\" ,.]+/',$search_query, $matches)) {
        return redirect()->back()->with('alert_danger','Please, remove any special characters and try searching again!');
      }
      $partner2 = $request->input('partner_country');
      $year_signed = $request->input('year_signed');
      $treaty_status = $request->input('treaty_status');
      $type_of_search = $request->input('type_of_search');
      $is_advanced = $request->input('is_advanced');
      $all_results = [];

      if ($request->isMethod('get')) {
        if ($is_advanced == True) {
          // Do the advanced filtering here
          if($partner2 == "all" && $treaty_status == "all" && !empty($year_signed)) {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                ->where('tts.signing_date', 'like', '%'.$year_signed.'%')
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
          else if ($partner2 == "all" && $treaty_status == "all") {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
          else if (is_numeric($partner2) && is_numeric($treaty_status)) {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                ->where('tts.partner2_id', $partner2)
                ->where('tts.treaty_status_id', $treaty_status)
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
          else if (is_numeric($partner2)) {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                ->where('tts.partner2_id', $partner2)
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
          else if (is_numeric($treaty_status)) {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                ->where('tts.treaty_status_id', $treaty_status)
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
          else {
            $tax_treaties_sections = TaxTreatySection::hydrate(\Searchy::search('tax_treaties_sections as ttsc')
                ->fields('ttsc.contents')
                ->query($search_query)
                ->select('ttsc.*')
                ->getQuery()
                ->join('tax_treaties_articles as tta', 'ttsc.tax_treaty_article_id', '=', 'tta.id')
                ->join('tax_treaties as tts', 'tta.tax_treaty_id', '=', 'tts.id')
                // ->where('tts.partner2_id', $partner2)
                // ->where('tts.treaty_status_id', $treaty_status)
                ->where('tts.publish_status_id', 41)
                ->having('relevance', '>', $this->relevance)
                ->get()->toArray()
            );
          }
        } 
        else {
          $tax_treaties = TaxTreaty::hydrate(
            \Searchy::tax_treaties('treaty_name', 'description')->query($search_query)->getQuery()
            ->where('publish_status_id', 41)
            ->having('relevance', '>', $this->relevance)
            ->get()->toArray()
          );
          $tax_treaties_articles = TaxTreatyArticle::hydrate(
            \Searchy:: tax_treaties_articles('article_name')->query($search_query)->getQuery()
              ->distinct('tax_treaty_id')
              ->having('relevance', '>', $this->relevance)
              ->get()->toArray()
          );
          $tax_treaties_sections = TaxTreatySection::hydrate(
            \Searchy::tax_treaties_sections('contents')->query($search_query)->getQuery()
            ->having('relevance', '>', $this->relevance)
            ->get()->toArray()
          );
          $all_results['tax_treaties_articles'] = $tax_treaties_articles;
          $all_results['tax_treaties'] = $tax_treaties;
        }
        // Organize the DB results
        $all_results['tax_treaties_sections'] = $tax_treaties_sections;

        // Create a well formatted final results array with the treaty name being the outer most key
        $final_results = [];
        $overall_results_count = 0;
        foreach($all_results as $treaty_key => $treaty_key_data) {
          foreach ($treaty_key_data as $inner_key => $actual_data) {
            if ($treaty_key == 'tax_treaties_articles') {
                $treaty_article = TaxTreatyArticle::with('tax_treaty')->where('id','=',$actual_data->id)->first();
                $treaty_article_name = $treaty_article->article_name;
                $treaty_name = $treaty_article->tax_treaty()->first()->treaty_name;

                if (!array_key_exists($treaty_name, $final_results)) {
                  $final_results[$treaty_name] = [];
                  if (!array_key_exists($treaty_article_name, $final_results[$treaty_name])) {
                    $final_results[$treaty_name][$treaty_article_name] = [];
                    array_push($final_results[$treaty_name][$treaty_article_name], $actual_data);
                    $overall_results_count += 1;
                  }
                  else {
                    if (!in_array($actual_data, $final_results[$treaty_name][$treaty_article_name])) {
                      array_push($final_results[$treaty_name][$treaty_article_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
                else {
                  if (!array_key_exists($treaty_article_name, $final_results[$treaty_name])) {
                    $final_results[$treaty_name][$treaty_article_name] = [];
                    array_push($final_results[$treaty_name][$treaty_article_name], $actual_data);
                    $overall_results_count += 1;
                  }
                  else {
                    if (!in_array($actual_data, $final_results[$treaty_name][$treaty_article_name])) {
                      array_push($final_results[$treaty_name][$treaty_article_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
              }
              else if ($treaty_key == 'tax_treaties_sections') {
                $treaty_section = TaxTreatySection::with('section_article')->where('id','=',$actual_data->id)->first();
                $parent_article = $treaty_section->section_article()->first();
                $parent_article_name = $treaty_section->section_article()->first()->article_name;
                $treaty_name = $parent_article->tax_treaty()->first()->treaty_name;

                if (!array_key_exists($treaty_name, $final_results)) {
                  $final_results[$treaty_name] = [];
                  if (!array_key_exists($parent_article_name, $final_results[$treaty_name])) {
                    $final_results[$treaty_name][$parent_article_name] = [];
                    array_push($final_results[$treaty_name][$parent_article_name], $actual_data);
                    $overall_results_count += 1;
                  }
                  else {
                    if (!in_array($actual_data, $final_results[$treaty_name][$parent_article_name])) {
                      array_push($final_results[$treaty_name][$parent_article_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
                else {
                  if (!array_key_exists($parent_article_name, $final_results[$treaty_name])) {
                    $final_results[$treaty_name][$parent_article_name] = [];
                    array_push($final_results[$treaty_name][$parent_article_name], $actual_data);
                    $overall_results_count += 1;
                  }
                  else {
                    if (!in_array($actual_data, $final_results[$treaty_name][$parent_article_name])) {
                      array_push($final_results[$treaty_name][$parent_article_name], $actual_data);
                      $overall_results_count += 1;
                    }
                  }
                }
              }
          }
        }
        if($type_of_search == 'quick_view') {
          return view('pages/treaty-search-simple-view', compact('treaties_status','countries','overall_results_count','final_results', 'paginate_no', 'optimus', 'search_query'));
        }
        return view('pages/treaty-search', compact('treaties_status','countries','overall_results_count','final_results', 'paginate_no', 'optimus', 'search_query'));
      }
    }
    /**
     * Return all the cases via an ajax call to the frontend.
     * 
     * @param  \Illuminate\Http\Request $request 
     * @return Json            
     */
    public function getAjaxResults(Request $request) {
        $optimus = Controller::optimus();

        $keywords_string = $request->input('term');
        $found_cases = $this->getResultsForKeyword($keywords_string);
        $suggestions = [];

        foreach ($found_cases as $key => $value) {
            $optimus_id = $optimus->encode($value->id);
            array_push(
                $suggestions, [
                'value'=>$optimus_id,
                'label'=>($value->plaintiff).' Vs. '.($value->defendant),
                'search_query'=>$keywords_string,
                'signal'=>$value->signal,
                'url'=>'/case_view/'.$optimus_id
                ]
            );
        };
        return json_encode(
            $suggestions
        );
    }
    /**
     * Do an in document search, given the doc ID and return matching sections for the provided
     * search query.
     * 
     * @param  \Illuminate\Http\Request $request 
     * @return Array An array of the matched sections as suggestions.
     */
    public function searchActVersion(Request $request) {
        $optimus = Controller::optimus();
        $suggestions = [];
        if( $request->input('version_id') == '' ) {
            return redirect()->back();
        }

        $act_version_id = $optimus->decode($request->input("version_id"));
        $search_query = $request->input('query');

        // get the section ids belonging to this version
        $all_sections = DB::table('act_sections')->join('act_parts', 'act_sections.act_part_id', '=', 'act_parts.id')->where('act_parts.act_version_id', '=', $act_version_id);
        $section_ids = $all_sections->pluck('act_sections.id')->toArray();

        // lets do a basic search since we are searching a single version
        $sections = ActSection::search($search_query)->whereIn('id', $section_ids)->get();
        $suggestions = array();
        foreach($sections as $ac_section){
            $optimus_id = $optimus->encode($ac_section['id']);

            $doc_type = $ac_section->part()->first()->doc_type;
            $location = $ac_section->part()->first()->title;
            $doc_type = ((($doc_type == 'act_part') ? 'Section' : ($doc_type == 'act_schedule')) ? 'Schedule' : 'Subsidiary');
            
            array_push($suggestions, [
                'search_query'=> $request->input('query'),
                'data'=> ['id' => $optimus_id, 'content' => $ac_section->content ],
                'value'=> "$doc_type: $ac_section->no. $ac_section->title",
                'act_version_id' => $ac_section->part()->first()->version()->first()->id,
                'type' => $doc_type
            ]);
        };

        return ["suggestions" => $suggestions];
    }
    /**
     * Return the acts in the system that match the given search query.
     * 
     * @param  \Illuminate\Http\Request $request 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getMainActAjaxResults(Request $request) {
      $optimus = Controller::optimus();
      $keywords_string = $request->input('term');
      $suggestions = [];

      $found_acts = Act::where('act_title', 'like', '%'.$keywords_string.'%')->get();

      foreach ($found_acts as $key => $value) {
          $optimus_id = $optimus->encode($value->id);
          array_push(
              $suggestions, [
              'value'=> "$value->act_title",
              'label'=> ($optimus_id),
              'search_query'=>$keywords_string,
              ]
          );
      };
      return ["suggestions" => $suggestions];
    }
    /**
     * Return the acts in the system that match the given search query.
     * 
     * @param  \Illuminate\Http\Request $request 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getActAjaxResults(Request $request) {
      $optimus = Controller::optimus();

      $keywords_string = $request->input('term');
      $found_acts = $this->getActsResultsForKeyword($keywords_string);

      $suggestions = [];

      foreach ($found_acts as $key => $value) {
          $optimus_id = $optimus->encode($value->id);
          array_push(
              $suggestions, [
              'value'=>$optimus_id,
              'label'=>($value->act_title),
              'search_query'=>$keywords_string,
              'year'=>($value->version_year),
              'url'=>'/laws-regulations/'.$optimus_id
              ]
          );
      };
      return json_encode(
          $suggestions
      );
    }
    /**
     * Return the Treaties in the system that match the given search query.
     * 
     * @param  \Illuminate\Http\Request $request 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getTreatyAjaxResults(Request $request) {
      $optimus = Controller::optimus();

      $keywords_string = $request->input('term');
      $found_acts = $this->getTreatiesResultsForKeyword($keywords_string);

      $suggestions = [];

      foreach ($found_acts as $key => $value) {
        $partner1 = SettingsVariables::where('id', '=', $value->partner1_id)->first();
        $partner1 =$partner1->variable_name;

        $partner2 = SettingsVariables::where('id', '=', $value->partner2_id)->first();
        $partner2 =$partner2->variable_name;

        $article_name = TaxTreatyArticle::where('tax_treaty_id', '=', $value->id)->first();
        $article_id = $article_name->id;

        $section = TaxTreatySection::where('tax_treaty_article_id', '=', $article_id)->first();
        $section_id = $section->id;

          $optimus_id = $optimus->encode($section_id);
          array_push(
              $suggestions, [
              'value'=>$optimus_id,
              'label'=>($value->treaty_name),
              'search_query'=>$keywords_string,
              'partner1'=>$partner1,
              'partner2'=>$partner2,
              'date_signed'=>($value->signing_date),
              'url'=>'/view_treaty_article?section_id='.$optimus_id
              ]
          );
      };
      return json_encode(
          // 'error'=>False, 
          $suggestions
          // ]
      );
    }
    /**
     * Return the acts in the system that match the given search query
     * 
     * @deprecated v1 Deprecated when migrating to v2 
     * @param  String  $keyword     
     * @param  boolean $paginate    
     * @param  integer $paginate_no 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getActsResultsForKeyword($keyword, $paginate=False, $paginate_no=5) {
      $results = DB::table('act_versions AS av')
            ->leftJoin('acts AS act', 'act.id', '=', 'av.act_id' )
            ->leftJoin('act_parts AS ap', 'av.id', '=', 'ap.act_version_id')
            ->leftJoin('act_divisions AS ad', 'ap.id', '=', 'ad.act_part_id')
            ->leftJoin('act_sections AS as', 'ap.id', '=', 'as.act_part_id')
            ->leftJoin('act_sections AS asd', 'ad.id', '=', 'asd.act_division_id')
            ->where('act.act_title', 'like', '%'.$keyword.'%')
            ->orwhere('av.preamble', 'like', '%'.$keyword.'%')
            ->orwhere('av.side_notes', 'like', '%'.$keyword.'%')
            ->orwhere('ap.title', 'like', '%'.$keyword.'%')
            ->orwhere('ad.title', 'like', '%'.$keyword.'%')
            ->orwhere('as.title', 'like', '%'.$keyword.'%')
            ->orwhere('asd.title', 'like', '%'.$keyword.'%')
            ->orwhere('as.content', 'like', '%'.$keyword.'%')
            ->orwhere('asd.content', 'like', '%'.$keyword.'%')
            ->orwhere('as.marginal_notes', 'like', '%'.$keyword.'%')
            ->orwhere('asd.marginal_notes', 'like', '%'.$keyword.'%')
            ->where('av.is_published', '=', 1)
            ->distinct('av.id AS id')
            ->select('av.id AS id', 'act.act_title', 'av.preamble', 'av.version_year', 'ap.id AS part_id', 'ap.title AS part_title', 'ad.id AS division_id', 'ad.title AS division_title', 'as.id AS sect_id', 'as.title AS section_title', 'as.content')
            ;
          if ($paginate) {
            return $results->paginate($paginate_no);
        }
      return $results->get();

    }
    /**
     * (OLD)   Return the treaties in the system that match the given search query.
     *
     * @deprecated v1 Deprecated when migrating to v2 
     * @param  String  $keyword     
     * @param  boolean $paginate    
     * @param  integer $paginate_no 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getTreatiesResultsForKeyword($keyword, $paginate=False, $paginate_no=5) {
      $results = DB::table('tax_treaties AS tr')
            ->leftJoin('settings_variables AS sv1', 'tr.partner1_id', '=', 'sv1.id')
            ->leftJoin('settings_variables AS sv2', 'tr.partner2_id', '=', 'sv2.id')
            ->leftJoin('tax_treaties_articles AS tta', 'tr.id', '=', 'tta.tax_treaty_id')
            ->leftJoin('tax_treaties_sections AS tts', 'tta.id', '=', 'tts.tax_treaty_article_id')
            ->where('tr.treaty_name', 'like', '%'.$keyword.'%')
            ->orWhere('tr.description', 'like', '%'.$keyword.'%')
            ->orWhere('sv2.variable_name', 'like', '%'.$keyword.'%')
            ->orWhere('sv1.variable_name', 'like', '%'.$keyword.'%')
            ->orWhere('tta.article_name', 'like', '%'.$keyword.'%')
            ->orWhere('tts.contents', 'like', '%'.$keyword.'%')
            ->where('tr.publish_status_id', '=', 41)
            ->DISTINCT('tr.id')
            ->select('tr.id AS id', 'tr.partner1_id', 'tr.partner2_id', 'tr.signing_date', 'tr.treaty_name', 'tr.description');
          if ($paginate) {
            return $results->paginate($paginate_no);
        }
      return $results->get();
    }
    /**
     * (OLD)   Return the cases in the system that match the given search query.
     *
     * @deprecated v1 Deprecated when migrating to v2 
     * @param  String  $keyword     
     * @param  boolean $paginate    
     * @param  integer $paginate_no 
     * @return Array An array of the matched acts as suggestions.
     */
    public function getResultsForKeyword($keyword, $paginate=False, $paginate_no=5) {
      $results = DB::table('court_cases AS cc')
             ->leftJoin('cases_judges AS cjs', 'cc.id','=','cjs.cases_id')
             ->leftJoin('courts AS cs', 'cc.court_id', '=', 'cs.id')
             ->leftJoin('court_locations AS cl', 'cs.court_location_id','=', 'cl.id')
             ->leftJoin('court_types AS ct', 'cs.court_type_id', '=', 'ct.id')
             ->leftJoin('judges AS js', 'cjs.judges_id', '=', 'js.id')
             ->where('cc.summary','like','%' .$keyword. '%')
             ->orWhere('cc.plaintiff', 'like', '%' .$keyword. '%')
             ->orWhere('cc.defendant', 'like', '%' .$keyword. '%')
             ->orWhere('cc.signal', 'like', '%' .$keyword. '%')
             ->orWhere('cc.case_name', 'like', '%' .$keyword. '%')
             ->orWhere('cc.case_number', 'like', '%' .$keyword. '%')
             ->orWhere('cc.judgement', 'like', '%' .$keyword. '%')
             ->orWhere('cjs.judgement', 'like', '%' .$keyword. '%')
             ->orWhere('cs.court_name', 'like', '%' .$keyword. '%')
             ->orWhere('cl.location_name', 'like', '%' .$keyword. '%')
             ->orWhere('ct.court_type', 'like', '%' .$keyword. '%')
             ->orWhere('js.judge_name', 'like', '%' .$keyword. '%')
             ->where('cc.is_published','=', 1)
             ->select('cc.id AS id', 'cc.case_number', 'cc.plaintiff', 'cc.defendant','cc.signal','cc.judgement_date', 'cs.court_name', 'cl.location_name');
        if ($paginate) {
            return $results->paginate($paginate_no);
        }
      return $results->get();
    }
    /**
     * Decode json in the provided array object.
     * 
     * @param  Array $arraywithrawjson 
     */
    public function decodeIncludedJson($arraywithrawjson) {
      foreach ($arraywithrawjson as $key => $value) {
        if ($this->is_JSON($value)) {
          $arraywithrawjson[$key]=json_decode($value);
        }
      }
    }
    /**
     * Check given string to ensure that it's json.
     * 
     * @param  String  $strigntocheck 
     * @return boolean  True if json false otherwise
     */
    public function is_JSON($strigntocheck) {
      json_decode($strigntocheck);
      return (json_last_error()===JSON_ERROR_NONE);
    }
}
