import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import { resolve } from 'path';
import { globSync } from 'glob';
// import react from '@vitejs/plugin-react';
// import vue from '@vitejs/plugin-vue';

export default defineConfig({
    build: {
        // cssMinify: 'lightningcss',
        rollupOptions: {
            output: {
                dir: 'public/build',
                compact: true,
            }
        },
        watch: true,
        manifest: 'manifest.json',
        minify: 'esbuild',
    },
    esbuild: {
        // pure: ['console.log'],
        minifyIdentifiers: false,
    },
    plugins: [
        laravel({
            input: globSync('resources/assets/{styles,scripts}/**/*.{css,js}'),
            refresh: true,
        }),
    ],
});