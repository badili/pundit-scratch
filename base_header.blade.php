<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>PwC Pundit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="PwC Pundit"> 
        <meta name="author" content="@badili_ke">
        <meta name="_token" content="{{ csrf_token() }}">

        <!-- Styles should not be included globally by default. Only limit the scripts to be included based on the page being accessed -->
        <link href="<?php echo asset('css/base.css'); ?>" rel="stylesheet">

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo asset('ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo asset('ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo asset('ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo asset('ico/apple-touch-icon-57-precomposed.png'); ?>">
        <link rel="shortcut icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">

        <link href="<?php echo asset('css/vendor.min.css'); ?>" rel="stylesheet">
        @yield('css')
    </head>
    @if(!Auth::user())
      <!-- /COOKIE ALERT -->
      <div id="cookie-alert" data-expire="30" class="alert alert-primary alert-position-bottom">
        <div class="container">
          <p>
            <i class="fa fa-warning"></i>
            We use cookies to personalize content and to provide you with an improved user experience. By continuing to browse this site you consent to the use of cookies. Please visit our <a href="{{ URL::route('pundit_pc') }}#cookie">privacy statement</a> for further details
          </p>
          <button type="button" data-dismiss="alert">
            <span class="cookie-close-btn" aria-hidden="true">I Understand</span>
            <span class="sr-only">Close</span>
          </button>
        </div>
      </div>
      <!-- /COOKIE ALERT -->
    @endif
    @yield('body')

    <!-- Scripts should not be included globally by default. Only limit the scripts to be included based on the page being accessed -->
    <!-- Global scripts-->
    <script src="<?php echo asset('js/vendor.js'); ?>"></script>
    @vite(['resources/assets/scripts/badili_common.js'])
    <!-- Global scripts-->
    @yield('scripts')
    <?php  
    // Get the sentry DSN for JS from the config
    $js_sentry_dsn = config('sentry.javascript_dsn');
    ?>
    <script type='text/javascript'>
        $(document).ready(function () {
            var pundit_help_link = "{!! URL::route('help_now') !!}";
            var pundit_subscribe_url = '{!! url('subscribe') !!}'
            var js_sentry_dsn = '{!!$js_sentry_dsn!!}';
            // Sentry
            Raven.config(js_sentry_dsn).install();

            // and run it again every time you scroll, tafadhal
            $(window).scroll(function() {
                 // badili_common.stickyNavigation();
            });
        });
    </script>
 
</html>
