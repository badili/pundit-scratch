<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        @yield('aimeos_header')
        <title>PwC Pundit</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="PwC Pundit"> 
        <meta name="author" content="@badili_ke">
        <meta name="_token" content="{{ csrf_token() }}">
        <!-- Styles should not be included globally by default. Only limit the scripts to be included based on the page being accessed -->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo asset('ico/apple-touch-icon-144-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo asset('ico/apple-touch-icon-114-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo asset('ico/apple-touch-icon-72-precomposed.png'); ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo asset('ico/apple-touch-icon-57-precomposed.png'); ?>">
        <link rel="shortcut icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">
        @yield('aimeos_styles')
        <link rel="shortcut icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo asset('ico/favicon.ico'); ?>" type="image/x-icon">
        
        <!-- Aimeos override CSS -->
        <link href="<?php echo asset('css/base.css'); ?>" rel="stylesheet">

    </head>
    <body>
    <header class="headroom inner_header">
        <div class="container">
          <div class="col-sm-1 header_logo">
            <img src="<?php echo asset('images/PwC_logo.png'); ?>" alt="logo">
          </div>
          <div class="col-sm-4 header_tag_line">
            <a href="{{ URL::route('index') }}"><h1>PwC Pundit</h1></a>
          </div>
          <div class="col-sm-6 col-md-push-1 menu_holder">
            <div class="user_header">
              <div class="user_menu_holder">
                @if(Auth::user())
                  <li>
                      <span class="user_icon"><img src="<?php echo asset('images/user_icon.svg'); ?>" alt="user icon"></span>
                      {{Auth::user()->firstname}} {{Auth::user()->lastname}}
                      <span class="down_icon"><img src="<?php echo asset('images/profile_arrow.svg'); ?>" alt="arrow"></span>
                      <ul>
                        <li><a href="#">My Bookmarks</a></li>
                        <li><a href="#">My Shopping Cart</a></li>
                        <li><a href="#">My Account</a></li>
                        <li><a href="{{route('dashboard')}}">Admin Dashboard</a></li>
                        <li><a href="{{route('user_signout')}}">Log Out</a></li>
                      </ul>
                  </li>
                @endif
              </div>
            </div>
            <ul class="header_main_menu_home">
              <li>
                <?php  
                  $laws_route_names = ['user_acts_parliament', 'all_user_acts', 'user_act_version_display', 'user_act_section_view'];
                ?>
                <a href="{{ URL::route('all_user_acts') }}" 
                @if(in_array(\Route::current()->getName(), $laws_route_names)) class="active"@endif>Laws &amp; Regulations</a>
              </li>
              <li>
                <a href="{{ URL::route('all_tax_treaties_ratified') }}" 
                <?php  
                  $treaties_route_names = ['tax_treaties_ratified', 'all_tax_treaties_ratified', 'view_treaty_article', 'tax_treaties_signed'];
                ?>
                @if(in_array(\Route::current()->getName(), $treaties_route_names)) class="active"@endif>Tax Treaties</a>
              </li>
              <li>
                <a href="{{ URL::route('all_cases') }}" 
                <?php  
                  $cases_route_names = [
                    'cases_court_appeal', 'cases_labor_employment', 'cases_high_court', 'cases_tax_appeal', 'cases_types', 'view_case', 'all_cases'];
                ?>
                @if(in_array(\Route::current()->getName(), $cases_route_names)) class="active"@endif>Cases</a>
              </li>
              <li>
                <?php 
                  $addons_routes = ['pn_addons', 'resource_addons', 'fbt_addons']
                ?>
                <a href="{{ route('pn_addons') }}" 
                @if(in_array(\Route::current()->getName(), $addons_routes)) class="active"@endif>Resources</a>
              </li>
              <li>
                <a href="{{ URL::route('aimeos_shop_list', ['locale' => 'en', 'currency' => 'KES']) }}" 
                  @if(\Route::current()->getName() == 'aimeos_shop_list') class="active" @endif>Store</a>
              </li>
            </ul>
          </div>
        </div>
    </header>
    <div class="container">
        <!-- breadcrumbs -->
        <ul class="breadcrumbs">
          <li><a href="{{ URL::route('index') }}">Home</a></li>
          <li><a href="{{ URL::route('aimeos_shop_list', ['locale' => 'en', 'currency' => 'KES']) }}" class="active">Store</a></li>
        </ul>
        @yield('content')
        <h1 class="page-header">Store</h1>
        <main class="col-md-8">
            @yield('aimeos_body')
        </main>
        <aside class="col-md-4 shopping-cart-aside">
            <div class="shop_aside_module">
              <div>
                @yield('aimeos_head')
                <!-- if no products show the below -->
                <!-- <p class="no-products-in-cart">No products in your cart</p> -->
                <!-- if we have products in cart show the below -->
              </div>
            </div>

            <div class="shop_aside_module products-categories">
              @yield('aimeos_aside')
            </div>

        </aside>
    </div>
    <!-- start footer -->
    @include('partials/v2/footer')
    <!-- support button -->
    @include('partials/v2/support_button')
    <!-- support button -->
    <!-- Global scripts-->
    <script src="<?php echo asset('bower/raven-js/dist/raven.min.js'); ?>" crossorigin="anonymous"></script>
    <!-- Joe's scripts-->
    <script src="<?php echo asset('js/headroom.js'); ?>"></script>
    <script src="<?php echo asset('js/vendor.js'); ?>"></script>
    <script>
        // Sentry
        Raven.config('https://677f6d2b6753485bb74e0a2baadcbbdb@sentry.badili.co.ke/8').install();
    </script>
</body>
</html>
