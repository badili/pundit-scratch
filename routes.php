<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested. 
|
*/

// Add new routes for Socialite to handle OAUTH 2.0
// Route::get('login/oauth',                   'Auth\LoginController@redirectToProvider')->name('oauth_2_pwc'); // To be removed in production
// Route::get('login/oauth/callback',          'Auth\LoginController@handleProviderCallback'); // To be removed in production

Route::get('login/oauth',                   'Auth\PwCIDLoginController@redirectToProvider')->name('oauth_2_pwc'); // To be enabled in production
Route::get('login/oauth/callback',          'Auth\PwCIDLoginController@handleProviderCallback'); // To be enabled in production

// Routes for the MPESA Logic
Route::post('/validate',                    'MPESAController@validateTransaction')->name('mpesa_validate');
Route::post('/confirm',                     'MPESAController@confirmTransaction')->name('mpesa_confirm');
Route::post('/bank/confirm',                'MPESAController@bankConfirmTransaction')->name('bank_confirm');

// General
Route::get('/',                             'Controller@index')->name('index');

//Route::get('/online-store', 'OnlineStoreController@index')->name('online_store');

// User authentication and signup
Route::get('/login',                                'UsersController@login_combined')->name('user_login');
Route::get('/signout',                              'UsersController@signOut')->name('user_signout');
Route::get('/signup',                               'UsersController@signUp')->name('user_signup');
Route::get('/forgot-password',                      'UsersController@forgotPassword')->name('forgot_password');
Route::post('/password-reset',                      'UsersController@restPassword')->name('reset_password');
Route::get('/signup-corporate',                     'UsersController@signUpCorporate')->name('user_signup_corporate');
Route::get('/signup-corporate-user',                'UsersController@signUpCorporateUser')->name('user_signup_corporate_user');


// content routes
Route::get('/act',                      function (){ return view('partials/act');                   	});
Route::get('/treaties-signed',          function (){ return view('pages/treaties-signed');          	});
Route::get('/treaty',                   function (){ return view('pages/treaty');                   	});
Route::get('/premium',                  function (){ return view('pages/premium');                  	});
Route::get('/case',                     function (){ return view('pages/case');                     	});
Route::get('/FAQ',                      function (){ return view('pages/faq');                      	});
Route::get('/faq-detailed',             function (){ return view('pages/faq-detailed');             	});
Route::get('/audit-trail',              function (){ return view('pages/audit-trail');              	});
Route::get('/editor-guide',             function (){ return view('pages/editor-guide');             	});
Route::get('/one-news',                 function (){ return view('pages/news-item3');               	});
Route::get('/one-news2',                function (){ return view('pages/news-item4');               	});
Route::get('/cases-tax-appeal-tribunal',function (){ return view('pages/cases-tax-appeal-tribunal');	});
Route::get('/addonspn',                 function (){ return view('pages/addons');                   	});
Route::get('/addons-benefits',          function (){ return view('pages/addons-benefits');          	});
Route::get('/addons-resources',         function (){ return view('pages/addons-resources');         	});
Route::get('/terms-conditions',         function (){ return view('v2/pages/pundit_tc');                 })->name('pundit_tc');
Route::get('/legal-disclaimer',         function (){ return view('v2/pages/legal-disclaimer');          })->name('pundit_ld');
Route::get('/privacy-commitment',       function (){ return view('v2/pages/privacy-commitment');        })->name('pundit_pc');
Route::get('/about',                    function (){ return view('v2/pages/about_halo');                })->name('about');
Route::get('/about-v1',                 function (){ return view('pages/about');                        })->name('about-v1');
Route::get('/contact-us',               function (){ return view('v2/pages/about_halo');                })->name('contact_us');
Route::get('/other-pwc-sites',          function (){ return view('pages/other-pwc-sites');              })->name('other_pwc_sites');

// Verification
Route::get('/verify/{email}/{verifyToken}',                         'Dashboard\UsersController@verifyUser')->name('send_mail_verification');
Route::get('/verify-corporate/{email_address}/{verifyToken}',       'Dashboard\CorporateController@verifyUser')->name('send_mail_corporate_verification');

Route::get('/shopping-cart',                        'OnlineStoreController@shoppingCart')->name('shopping_cart');

// divs sections
Route::get('/div_section',                          'Dashboard\DivSectionsController@getDivSection')->name('div_section');
Route::get('/active_button',                        'Dashboard\AddonsController@active_button')->name('dashboard_pagination_search');
Route::get('/file/{id}/{name}',                     'Dashboard\AddonsController@download_file')->name('download_file');

// event rules
Route::post('/get_events',                          'Dashboard\EventsController@get_events')->name('get_events');
Route::get('/calendar',                             'Dashboard\EventsController@calendar')->name('calendar');

// Bookmarked namespaces
Route::group(['namespace' => 'Bookmarks'], function() {
    Route::get('/bookmarks-laws-regulations',       'LawsRegulationsController@index')->name('bookmarks_laws_regulations');
    Route::get('/bookmarks-laws-regulations-eac',   'LawsRegulationsController@listEACActs')->name('bookmarks_laws_regulations_eac');
    Route::get('/bookmarks-tax-treaties',           'TaxTreatiesController@index')->name('bookmarks_tax_treaties');
    Route::get('/bookmarks-cases',                  'CasesController@index')->name('bookmarks_cases');
    Route::get('/bookmarks-addons',                 'AddonsController@index')->name('bookmarks_addons');
});

Route::get('/all-news',                             'Dashboard\NewsController@all_news')->name('all-news');

// Front-end namespaces
Route::get('/all-cases',                            'FrontEnd\CasesController@all_cases')->name('all_cases');
Route::get('/all-laws-regulations',                 'FrontEnd\LawsRegulationsController@listAllActs')->name('all_user_acts');
Route::get('/all-cases-type',                       'FrontEnd\CasesController@cases_by_type')->name('cases_types');
// End all the new look

Route::get('/laws-regulations',                     'FrontEnd\LawsRegulationsController@listActs')->name('user_acts_parliament');
Route::get('/laws-regulations-eac',                 'FrontEnd\LawsRegulationsController@listEACActs')->name('user_acts_parliament_eac');
Route::get('/search-laws-regulations',              'FrontEnd\LawsRegulationsController@searchActs')->name('search-laws-regulations');

Route::get('/tax-treaties',                         'Dashboard\TaxTreatiesController@all_treaties')->name('all_tax_treaties_ratified');
Route::get('/tax-treaties-ratified',                'Dashboard\TaxTreatiesController@ratified')->name('tax_treaties_ratified');
Route::get('/treaty_section',                       'Dashboard\TaxTreatiesController@getTreatySection')->name('treaty_section_fetch');
Route::get('/tax_treaties_signed',                  'Dashboard\TaxTreatiesController@signed')->name('tax_treaties_signed');

// consider taking behind authentication
Route::get('/search-treaty',                        'Dashboard\TaxTreatiesController@searchTreatiesFrontend')->name('treaty_search');
Route::get('/act/{id}',                             'Dashboard\ActVersionsController@getActVersionsDisplayPage')->name('user_act_version_display');
Route::get('/view-event/{id}',                      'Controller@link_event')->name('view_event');
Route::post('/add-new-corporate',                   'Dashboard\CorporateController@add_corporate')->name('add_corporate');

// content rules
Route::get('/full-calendar',                        'Dashboard\EventsController@calendar')->name('full-calendar');
Route::get('/search-results',                       function (){ return view('pages/search'); });


Route::get('/cases-court-appeal',                   'FrontEnd\CasesController@court_appeal')->name('cases_court_appeal');
Route::get('/cases-high-court',                     'FrontEnd\CasesController@high_court')->name('cases_high_court');
Route::get('/cases-tax-appeal',                     'FrontEnd\CasesController@tax_appeal')->name('cases_tax_appeal');
Route::get('/cases-supreme-court',                  'FrontEnd\CasesController@supreme_court')->name('cases-supreme-court');
Route::get('/cases-labor-employment',               'FrontEnd\CasesController@labor_court')->name('cases_labor_employment');

Route::get('/online-store',                         'FrontEnd\OnlineStoreController@index')->name('online-store-front'); 
Route::get('/all_addons_web',                       'FrontEnd\AddonsController@all_add_ons')->name('addons_front_all'); 


//post user data with csrf protection
Route::group(array('before' => 'csrf'), function () {
    // Process Search results for Cases
    Route::get('/search-cases',                 'MainSearchController@searchCases')->name('search_cases');
    Route::get('/search_acts',                  'MainSearchController@searchActsV2')->name('search_acts');
    Route::get('/search_treaties',              'MainSearchController@SearchTreaties')->name('search_treaties');
    Route::get('/cases_autocomplete_search',    'MainSearchController@getAjaxResults')->name('search_cases_ajax');
    Route::get('/acts_autocomplete_search',     'MainSearchController@getActAjaxResults')->name('search_acts_ajax');
    Route::get('/main_acts_autocomplete_search','MainSearchController@getMainActAjaxResults')->name('main_search_acts_ajax');
    Route::get('/treaties_autocomplete_search', 'MainSearchController@getTreatyAjaxResults')->name('search_treaties_ajax');
    Route::get('/act_version_search',           'MainSearchController@searchActVersion')->name('act_version_search');
    Route::get('/search_treaty_v2',             'MainSearchController@searchTreatyV2')->name('search_treaties_v2');
    Route::get('/search_cases_v2',              'MainSearchController@searchCasesV2')->name('search_cases_v2');
    Route::post('/search_addons',               'Dashboard\AddonsController@searchAddons')->name('search_addons');
    
    // Send help now route and feedback
    Route::post('/feedback',                        'FeedBackHelpController@feedback')->name('submit_feedback');
    Route::post('/help_now',                        'FeedBackHelpController@helpnow')->name('help_now');
    // Route for the contact us post with data
    Route::post('/contact_us_post', 'Controller@contactUs')->name('send_contact_us');
    //sign up
    Route::post('/signup_corporate',                'Dashboard\CorporateController@add_corporate')->name('signup_corporate');
    Route::post('/signup-user',                     'Dashboard\UsersController@add_user')->name('user_signup_corporate_post');
    Route::post('/signup-corporate-user',           'UsersController@signUpCorporateUser')->name('user_signup_corporate_user_post');
    //sign in
    Route::post('/signin',                          'UsersController@signIn')->name('user_signin_post');
    Route::get('/signin',                           'UsersController@signIn')->name('user_signin');
    // Second subscription step
    Route::post('/subscription2',                   'UsersController@subscriptions_step2')->name('subscriptions_step2');
    Route::post('/subscription3',                   'UsersController@subscriptions_step3')->name('subscriptions_step3');
}); 

Route::group(['middleware' => ['auth']], function() {
    Route::get('/news',                             'Dashboard\NewsController@index')->name('news_items_front');
    Route::get('/subscription',                     'UsersController@subscriptions')->name('subscriptions');
    Route::get('/refresh_sub_payment',              'UsersController@refresh_sub_payment')->name('refresh_sub_payment');
    Route::get('/my_account',                       'UsersController@my_account')->name('my_account');
    Route::post('/update_account',                  'UsersController@updateAccount')->name('update_account');
    Route::post('/change_password',                 'UsersController@change_password')->name('change_password');
    Route::get('/get_user',                         'Dashboard\UsersController@get_edit_user')->name('dashboard_get_user');
    
    Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function() {
        Route::get('/div_section',                  'DivSectionsController@getDivSection')->name('dashboard_div_section');       
        Route::get('/file/{id}/{name}',             'AddonsController@download_file')->name('addons_download_file');
    });
    
    Route::group(['prefix' => 'v1'], function() {
        // My account
        Route::get('/my_account',                   'UsersController@my_account')->name('v1_my_account');
    });
});

Route::group(['middleware' => ['auth', 'some_admin']], function() {
    // get dashboard page
    Route::get('/dashboard',                        'DashboardController@index')->name('dashboard');
});

Route::group(['middleware' => ['auth', 'some_admin']], function() {
    // dashboard namespaces
    Route::get('/dashboard',                        'DashboardController@index')->name('dashboard');
    Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function() {
        // users routes  
        Route::get('/all-users',                    'UsersController@all_users')->name('dashboard_all_users');
        Route::get('/personal-users',               'UsersController@personal_users')->name('dashboard_personal_users');
        Route::get('/corporates-users',             'UsersController@corporates_users')->name('dashboard_corporates_users');
        Route::get('/admin-users',                  'UsersController@admin_users')->name('dashboard_admin_users');
        Route::get('/get_edit',                     'UsersController@get_edit_user')->name('dashboard_get_edit_users');
        Route::post('/add-new-user',                'UsersController@add_user')->name('dashboard_new_user');
        Route::get('/action_checkboxs_users',       'UsersController@checked_boxes')->name('dashboard_edit_users');
        Route::get('/update-user',                  'UsersController@updating_user')->name('dashboard_update_users');
        Route::get('/admin-button',                 'UsersController@tab_admin')->name('dashboard_admin_button');

        // setting Routes 
        Route::get('/all_settings',                 'SettingsController@all_settings')->name('dashboard_settings');
        Route::post('/add_setting',                 'SettingsController@add_setting')->name('dashboard_new_setting');
        Route::get('/fetch_setting',                'SettingsController@fetch_setting')->name('dashboard_fetch_setting');
        Route::get('/get_edit_settings',            'SettingsController@get_edit')->name('dashboard_get_edit_settings');
        Route::get('/edit_setting',                 'SettingsController@edit_setting')->name('dashboard_edit_setting');
        Route::get('/action_checkboxs_settings',    'SettingsController@action_checkboxs')->name('dashboard_action_checkboxs_settings');

        // corporates Routes 
        Route::get('/all_corporates',               'CorporateController@corporates')->name('dashboard_all_corporates');
        Route::post('/add-new-corporate',           'CorporateController@add_corporate')->name('dashboard_add_corporate');
        Route::get('/get_edit_corporate',           'CorporateController@get_edit')->name('dashboard_get_edit_corporate');
        Route::get('/update_corporate',             'CorporateController@update_corporate')->name('dashboard_update_corporates');
    });
});

Route::group(['middleware' => ['auth', 'subscribed']], function() {
    // newsletter subscription
    Route::post('/subscribe',                                       'Dashboard\SubscriptionsController@subscribe')->name('subscribe');
    Route::get('/view-news/{id}',                                   'Dashboard\NewsController@get_news_item')->name('view_news');

    // laws and regulations
    Route::get('/laws-regulations/{id}',                            'FrontEnd\LawsRegulationsController@showActVersion')->name('user_act_version_display');
    Route::post('/laws-regulations/get_xref',                       'Dashboard\CrossReferenceController@fetchXrefSection')->name('lr_get_xref');
    Route::post('/laws-regulations/fetch_bookmark_meta_data',       'BookmarksController@fetchBookmarkMetaData')->name('fetch_bookmark_meta_data_lr');

    Route::get('/act_section',                                      'Dashboard\SectionsController@getActSection')->name('user_act_section_fetch');
    Route::get('/act_section',                                      'Dashboard\SectionsController@getActSection')->name('user_act_section_fetch');


    Route::get('/view_act_section',                                 'Dashboard\SectionsController@viewActSection')->name('user_act_section_view');
    Route::get('/view_act_part',                                    'Dashboard\SectionsController@viewActPart')->name('user_act_part_view');
    Route::get('/act_preamble/{id}',                                'Dashboard\ActVersionsController@getActVersionPreamble')->name('act_version_preamble');
    
    Route::get('/part_division/{id}',                               'Dashboard\ActVersionsController@getPartDivisions')->name('act_part_division');

    Route::post('/case_view/fetch_bookmark_meta_data',              'BookmarksController@fetchBookmarkMetaData')->name('fetch_bookmark_meta_data_case');
    Route::get('/case_view/div_section',                            'Dashboard\DivSectionsController@getDivSection')->name('dashboard_div_section_case');
    Route::get('/case_view/{id}',                                   'Dashboard\CourtCaseController@viewCase')->name('view_case');
    Route::post('/update_case_judge_judgement',                     'Dashboard\CourtCaseController@updateCaseJudgeJudgement')->name('update_case_judge_judgement');

    Route::get('/corporate-details',                'Dashboard\CorporateController@corporate_details')->name('dashboard_my_coporate');
    Route::get('/corporate-users',                  'Dashboard\UsersController@my_users')->name('dashboard_my_users');
    Route::get('/action_checkboxs_my_users',        'Dashboard\UsersController@checked_boxes')->name('dashboard_checkboxes_my_users');
    Route::post('/add-my-user',                     'Dashboard\UsersController@add_user')->name('dashboard_my_new_user');
    Route::get('/update-my-user',                   'Dashboard\UsersController@updating_user')->name('dashboard_update_my_users');
    Route::get('/user/create',                      'Dashboard\UsersController@create_corporate_user')->name('create_corporate_user');
    Route::post('/add_corporate_user',              'Dashboard\UsersController@add_user')->name('dashboard_corprate_user');
    Route::get('/update_corporate_user',            'Dashboard\UsersController@updating_user')->name('dashboard_update_corporate_users');

    Route::get('/treaty/{id}',                      'Dashboard\TaxTreatiesController@populateTreatyContent')->name('view_treaty');
    Route::get('/treaty_article',                   'Dashboard\TaxTreatiesController@getTreatyArticle')->name('fetch_treaty_article');
    Route::get('/view_treaty_article',              'Dashboard\TaxTreatiesController@viewTreatyArticle')->name('view_treaty_article');
    Route::get('/get_treaty_section',               'Dashboard\TaxTreatiesController@getTreatySection')->name('normal_treaty_section_fetch');
    Route::post('/get_xref',                        'Dashboard\CrossReferenceController@fetchXrefSection')->name('get_xref');

    Route::post('/save_bookmark',                       'BookmarksController@saveBookmark')->name('save_bookmark');
    Route::post('/delete_bookmark',                     'BookmarksController@deleteBookmark')->name('delete_bookmark');
    Route::get('/fetch_bookmark',                       'BookmarksController@fetchBookmarks')->name('fetch_bookmark');
    Route::post('/fetch_bookmark_meta_data',             'BookmarksController@fetchBookmarkMetaData')->name('fetch_bookmark_meta_data');
    Route::get('/add-ons',                              'Dashboard\AddonsController@public_notices')->name('pn_addons');
    Route::get('/add-ons-fbt',                          'Dashboard\AddonsController@fbt')->name('fbt_addons'); 
    Route::get('/add-ons-resource',                     'Dashboard\AddonsController@resource')->name('resource_addons');
});

Route::group(['middleware' => ['auth', 'content_admin']], function() {
    //act version
    Route::get('/dashboard',                        'DashboardController@index')->name('dashboard');
    Route::get('/act/{id}',                         'Dashboard\ActVersionsController@getActVersionsDisplayPage')->name('act_version_display');
    
    // Dashboard namespaces
    Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function() {
        Route::post('/autocomplete',                'AutocompleteController@trafficController')->name('dashboard_autocomplete');

        //act version
        Route::get('/act/{id}',                     'ActVersionsController@getActVersionsDisplayPage')->name('dashboard_act_version_display');
        Route::get('/act_preamble/{id}',            'ActVersionsController@getActVersionPreamble')->name('dashboard_act_version_preamble');
        Route::post('/act/save_preamble',           'ActVersionsController@saveActVersionPreamble')->name('dashboard_save_version_preamble');
    
        Route::post('/act/parse',                   'SectionsController@parseDocument')->name('dashboard_act_parse_doc');

        Route::get('/acts-parliament',              'ActsController@getAdminActsParliament')->name('dashboard_acts_parliament');
        // Redudant route for acts of parliament. To be deleted progressively
        Route::get('/acts',                         'ActsController@getAdminActsParliament')->name('old_dashboard_acts_parliament');
        Route::get('/acts-eac',                     'ActsController@getAdminActsEAC')->name('dashboard_acts_eac');

        Route::get('/clear-session',                'ActsController@clearSession')->name('dashboard_clear_session');

        Route::get('/acts/widget',                  'ActsController@createWidget')->name('dashboard_acts_widget');
        Route::post('/act/save_edits/{id}',         'SectionsController@saveUserEdits')->name('acts_save_edits');
        Route::get('/acts_search',                  'ActsController@searchActs')->name('acts_search');

        // acts section
        Route::get('/act_section',                  'SectionsController@getActSection')->name('dashboard_act_section_fetch');
        Route::get('/content_builder/{id}',         'SectionsController@getContentBuilderPage')->name('content_builder');
        Route::get('/act/content_builder/{id}',     'SectionsController@getContentBuilderPage')->name('act_content_builder');
        Route::get('/view_act_section',             'SectionsController@viewActSection')->name('dashboard_act_section_view');
        Route::get('/view_act_part',                'SectionsController@viewActPart')->name('dashboard_act_part_view');
        Route::get('/setting_variables',            'SettingsController@settings_pwc')->name('dashboard_all_dropDown_users');
        Route::get('/subsidiary',                   'ActsController@getAdminActsParliament')->name('dashboard_subsidiary');

        // references
        Route::get('/clear_xreferences',            'CrossReferenceController@clearAllCrossReferences')->name('clear_all_references');
        Route::get('/process_xreferences',          'CrossReferenceController@processCrossReferences')->name('update_xreferences');

        // schedules
        Route::get('/schedule_builder/{id}',        'ActVersionsController@getScheduleBuilderPage')->name('schedule_builder');
        Route::get('/subsidiary_builder/{id}',      'ActVersionsController@getSubsidiaryBuilderPage')->name('subsidiary_builder');

        Route::post('/save_appendice_part/{id}',    'ActVersionsController@saveAppendicePart')->name('save_appendice_part');
        Route::post('/save_appendice_section/{id}', 'ActVersionsController@saveAppendiceSection')->name('save_appendice_section');

        Route::post('/define_schedule/{id}',        'ActVersionsController@defineScheduleParts')->name('define_schedule_parts'); 
        Route::post('/save_schedule/{id}',          'ActVersionsController@saveScheduleParts')->name('save_schedule_parts');
        Route::post('/acts/save_part_details/{id}', 'ActVersionsController@savePartDetails')->name('dashboard_save_part_details');
        Route::post('/acts/add_new_part',           'ActVersionsController@addNewPart')->name('dashboard_add_new_part');
        Route::post('/acts/add_new_division',       'ActVersionsController@addNewDivision')->name('dashboard_add_new_division');
        Route::post('/act/add_new_section',         'SectionsController@saveNewSection')->name('dashboard_add_new_section');
        Route::post('/acts/save_division_details/{id}', 'ActVersionsController@saveDivisionDetails')->name('dashboard_save_division_details');
        Route::post('/acts/save_section_details/{id}', 'ActVersionsController@saveSectionDetails')->name('dashboard_save_section_details');
        Route::post('/acts/save_schedule_details/{id}', 'ActVersionsController@saveScheduleDetails')->name('dashboard_save_schedule_details');
        Route::post('/acts/save_subsidiary_details/{id}', 'ActVersionsController@saveSubsidiaryDetails')->name('dashboard_save_subsidiary_details');
  
        // cases
        Route::get('/cases',                        'CourtCaseController@casesHome')->name('dashboard_cases');
        Route::get('/new_case',                     'CourtCaseController@newCase')->name('dashboard_new_case');
        Route::post('/save_case',                   'CourtCaseController@saveCase')->name('dashboard_save_case');
        Route::get('/case/{id}',                    'CourtCaseController@viewCase')->name('dashboard_view_case');
        Route::get('/case/edit/{id}',               'CourtCaseController@getCaseEditPage')->name('dashboard_edit_case');
        
        // Publish cases selected
        Route::post('/cases/publish',               'CourtCaseController@publishMany')->name('cases_publish_post');
        // Unpublish many Cases selected
        Route::post('/cases/unpublish',             'CourtCaseController@unPublishMany')->name('cases_unpublish_post');
        Route::get('/court_case/delete',            'CourtCaseController@deleteCase')->name('court_case_delete');

        // add-ons routes 
        Route::get('/addons',                       'AddonsController@public_notices')->name('dashboard_addons_pn');
        Route::get('/get_fbt_data',                 'AddonsController@get_fbt_data')->name('dash_get_fbt_data');
        Route::get('/addons-fbt',                   'AddonsController@fbt')->name('dashboard_addons_fbt');
        Route::get('/addons-resource',              'AddonsController@resource')->name('dashboard_addons_resource');

        Route::post('/add_new_addons',               'AddonsController@add_addons')->name('dashboard_new_addons');
        Route::post('/add_new_fbt',                  'AddonsController@add_fbts')->name('dashboard_new_fbt');
        Route::get('/edit_fbts',                     'AddonsController@edit_fbts')->name('dashboard_edit_fbt');
        Route::get('/get_edit_addons',               'AddonsController@get_edit_addons')->name('dashboard_get_edit_addons');
        Route::get('/serach_addon_auto',             'AddonsController@auto_complete')->name('dashboard_auto_search_addons');
        Route::get('/edit_addons',                   'AddonsController@edit_addons')->name('dashboard_edit_addons');
        Route::get('/action_checkboxs_addons',       'AddonsController@action_checkboxs')->name('dashboard_action_checkboxs_addons');
        Route::get('/button-search',                 'AddonsController@result_search')->name('dashboard_addon_up_search');
        Route::get('/active_button',                 'AddonsController@active_button')->name('dashboard_active_search');

        // event  routes
        Route::get('/dashboard_search-treaty',      'TaxTreatiesController@searchTreaties')->name('dashboard_search-treaty');

        Route::get('/events',                       'EventsController@all_events')->name('dashboard_all_event');
        Route::post('/add_event',                   'EventsController@add_event')->name('dashboard_add_event');
        Route::get('/serach_event_auto',            'EventsController@seacrch_auto_event')->name('dashboard_auto_search_event');
        Route::get('/get_edit_event',               'EventsController@edit_event')->name('dashboard_event_get_edit');
        Route::get('/action_checkboxs_events',      'EventsController@checked_boxes')->name('dashboard_edit_events');
        Route::get('/update_event',                 'EventsController@updating_event')->name('dashboard_update_events');

        // news Routes
        Route::get('/news',                         'NewsController@news')->name('dashboard_news');
        Route::get('/all_news',                     'NewsController@all_news')->name('dashboard_all_news');
        Route::post('/add_news',                    'NewsController@add_news')->name('dashboard_add_news');
        Route::get('/get_edit_news',                'NewsController@edit_news')->name('dashboard_edit_news');
        Route::get('/action_checkboxs_news',        'NewsController@checked_boxes')->name('dashboard_checked_boxes');
        Route::get('/serach_news_auto',             'NewsController@seacrch_auto_news')->name('dashboard_auto_search_news');
        Route::get('/update_news',                  'NewsController@updating_news')->name('dashboard_update_news');
        Route::get('/get_news_snippet',             'NewsController@get_news_snippets')->name('get_news_snippets');

        // act instantiate routes
        Route::post('/act_roll_over',               'ActVersionsController@rollOverAct')->name('act_roll_over');
        Route::get('/acts/create/1',                'ActVersionsController@createActSelectPage')->name('dashboard_create_act_1');
        Route::get('/acts/create/2',                'ActVersionsController@createActVersionPage')->name('dashboard_create_act_2');
        Route::get('/acts/create/2/back',           'ActVersionsController@backToStep1')->name('dashboard_create_act_2_back');
        Route::get('/acts/create/3',                'PartsController@createAbsolutePartNumberPage')->name('dashboard_create_act_3');
        Route::get('/acts/create/3/back',           'PartsController@backToStep2')->name('dashboard_create_act_3_back');
        Route::get('/acts/create/3b',               'PartsController@createPartDefinitionPage')->name('dashboard_create_act_3b');
        Route::get('/acts/create/3b/back',          'PartsController@backToStep3')->name('dashboard_create_act_3b_back');
        Route::get('/acts/create/3c',               'DivisionsController@createDivisionsDefinitionPage')->name('dashboard_create_act_3c');
        Route::get('/acts/create/3d',               'DivisionsController@createPreviewAnatomyPage')->name('dashboard_create_act_3d');
        Route::get('/acts/create/4',                'PartsController@createAssignContributorsPage')->name('dashboard_create_act_4');
        Route::get('/acts/create/reset',            'ActsController@reset')->name('dashboard_create_act_reset');

        // edits an act version
        Route::get('/act/edit/{id}',                'ActVersionsController@getActVersionsEditPage')->name('dashboard_act_version_edit');
        Route::get('/act/structure/{id}',           'ActVersionsController@getActVersionsStructure')->name('dashboard_act_version_structure');

        // publish an act version on the acts dashboard
        Route::get('/act/version/publish',          'ActVersionsController@publishOne')->name('act_version_publish_post');
        // unpublish an act version on the acts dashboard
        Route::get('/act/version/unpublish',        'ActVersionsController@unPublishOne')->name('act_version_unpublish_post');
        // delete an act version on the acts dashboard
        Route::get('/act/version/delete',           'ActVersionsController@deleteAct')->name('act_version_delete');
        Route::get('/act/part/delete',              'ActVersionsController@deletePart')->name('act_part_delete');
        Route::get('/act/section/delete',           'ActVersionsController@deleteSection')->name('act_section_delete');
        Route::post('/act/division/delete',         'ActVersionsController@deleteDivision')->name('act_division_delete');
        Route::get('/act/schedule/delete',          'ActVersionsController@deleteSchedule')->name('act_schedule_delete');

        // treaties instantiate routes
        Route::get('/treaty/{id}',                  'TaxTreatiesController@populateTreatyContent')->name('dashboard_treaty');
        Route::get('/treaty_section',               'TaxTreatiesController@getTreatySection')->name('dashboard_treaty_section_fetch');
        Route::get('/tax-treaties',                 'TaxTreatiesController@ratified')->name('dashboard_tax_treaties');
        Route::post('/save-treaty',                 'TaxTreatiesController@saveTreaty')->name('dashboard_save_treaty');
        Route::get('/tax_treaty/delete',            'TaxTreatiesController@deleteTreaty')->name('tax_treaty_delete');
        Route::get('/create-treaty',                'TaxTreatiesController@createNewTreaty')->name('dashboard_create_treaty');
        Route::get('/populate-treaty/{id}',         'TaxTreatiesController@populateTreatyContent')->name('dashboard_populate_treaty');
        Route::post('/treaty/save_edits/{id}',      'TaxTreatiesController@saveUserEdits')->name('dashboard_treaty_save_edits');
        Route::get('/view_treaty_article',          'TaxTreatiesController@viewTreatyArticle')->name('dashboard_view_treaty_article');
        Route::post('/treaty/save_article_details/{id}', 'TaxTreatiesController@saveArticleDetails')->name('dashboard_save_article_details');
        Route::get('/treaty/article/delete',          'TaxTreatiesController@deleteArticle')->name('treaty_article_delete');
        
        Route::get('/tax-treaties-ratified',        'TaxTreatiesController@ratified')->name('dashboard-tax-treaties-ratified');
        Route::get('/tax_treaties_signed',          'TaxTreatiesController@signed')->name('dashboard-tax-treaties-signed');

        // Publish Tax Treaties selected
        Route::post('/tax-treaties/publish',        'TaxTreatiesController@publishMany')->name('tax_treaties_publish_post');
        // Unpublish many Tax Treaties selected
        Route::post('/tax-treaties/unpublish',      'TaxTreatiesController@unPublishMany')->name('tax_treaties_unpublish_post');
        Route::get('/treaty/edit/{id}',             'TaxTreatiesController@getTreatyEditPage')->name('dashboard_treaty_edit');
        

        Route::group(array('before' => 'csrf'), function () {
            Route::post('/acts/widget',             'ActsController@postCreateWidget')->name('dashboard_acts_widget_post');
            // act instantiate routes
            Route::post('/acts/create/1',           'ActVersionsController@postActSelectPage')->name('dashboard_create_act_1_post');
            Route::post('/acts/create/2',           'ActVersionsController@postActVersionPage')->name('dashboard_create_act_2_post');
            Route::post('/acts/create/3',           'PartsController@postAbsolutePartNumberPage')->name('dashboard_create_act_3_post');
            Route::post('/acts/create/3b',          'PartsController@postPartDefinitionPage')->name('dashboard_create_act_3b_post');
            Route::post('/acts/create/3c',          'DivisionsController@postDivisionsDefinitionPage')->name('dashboard_create_act_3c_post');
            Route::post('/acts/create/3d',          'DivisionsController@postPreviewAnatomyPage')->name('dashboard_create_act_3d_post');
            Route::post('/acts/create/4',           'PartsController@postAssignContributorsPage')->name('dashboard_create_act_4_post');

            // publish many acts versions selected
            Route::post('/acts/versions/publish',   'ActVersionsController@publishMany')->name('act_versions_publish_post');
            //unpublish many acts versions selected
            Route::post('/acts/versions/unpublish', 'ActVersionsController@unPublishMany')->name('act_versions_unpublish_post');

            // acts edit routes
            Route::post('/acts/edit/main_info',     'ActVersionsController@postEditMainInfo')->name('dashboard_act_version_edit_main_info_post');
            Route::post('/treaty/edit/main_info',   'TaxTreatiesController@postEditMainInfo')->name('dashboard_edit_treaty_main_info');
            
            // Edgar routes, to be confirmed in terms of routing corporates
            Route::get('/treaty',                   'TaxTreatiesController@getEdgarView')->name('dashboard_treaties');

            // content builder commit
            Route::post('/content_builder/commit',  'SectionsController@postContentBuilderCommit')->name('content_builder_commit');

            //content builder commit with error
            Route::post('/content_builder/commit_with_error', 'SectionsController@postContentBuilderCommitWithError')->name('content_builder_commit_with_error');
        });
    });
 
    // All the new look and feel will be nested here
    Route::group(['prefix' => 'v1'], function() {
        Route::get('/home',                             'Controller@index')->name('v1_index');
        
        // Cases
        Route::get('/cases-court-appeal',               'FrontEnd\CasesController@court_appeal')->name('v1_cases_court_appeal');
        Route::get('/cases-labor-employment',           'FrontEnd\CasesController@labor_court')->name('v1_cases_labor_employment');
        Route::get('/cases-tax-appeal',                 'FrontEnd\CasesController@tax_appeal')->name('v1_cases_tax_appeal');
        Route::get('/cases-high-court',                 'FrontEnd\CasesController@high_court')->name('v1_cases_high_court');
        Route::get('/case_view/{id}',                   'Dashboard\CourtCaseController@viewCase')->name('v1_view_case');
        // Laws and Regulations
        Route::get('/laws-regulations',                 'FrontEnd\LawsRegulationsController@listActs')->name('v1_user_acts_parliament');
        Route::get('/laws-regulations-eac',             'FrontEnd\LawsRegulationsController@listEACActs')->name('v1_user_acts_parliament_eac');
        Route::get('/laws-regulations/{id}',            'FrontEnd\LawsRegulationsController@showActVersion')->name('v1_user_act_version_display');
        Route::get('/view_act_section',                 'Dashboard\SectionsController@viewActSection')->name('v1_user_act_section_view');
        Route::get('/view_act_part',                    'Dashboard\SectionsController@viewActPart')->name('v1_user_act_part_view');
        Route::get('/act_section',                      'Dashboard\SectionsController@getActSection')->name('v1_user_act_section_fetch');
    
        // Tax treaties
        Route::get('/tax-treaties-ratified',            'Dashboard\TaxTreatiesController@ratified')->name('v1_tax_treaties_ratified');
        Route::get('/view_treaty_article',              'Dashboard\TaxTreatiesController@viewTreatyArticle')->name('v1_view_treaty_article');
        Route::get('/tax_treaties_signed',              'Dashboard\TaxTreatiesController@signed')->name('v1_tax_treaties_signed');
        // Resources
        Route::get('/add-ons',                          'Dashboard\AddonsController@public_notices')->name('v1_pn_addons');
        Route::get('/add-ons-fbt',                      'Dashboard\AddonsController@fbt')->name('v1_fbt_addons'); 
        Route::get('/add-ons-resource',                 'Dashboard\AddonsController@resource')->name('v1_resource_addons');

        // News
        Route::get('/news',                             'Dashboard\NewsController@index')->name('v1_news_items_front');
        Route::get('/view-news/{id}',                   'Controller@link_news')->name('v1_view_news');
    });
});

// Route::post('/signin',      'UsersController@signIn')->name('v2_user_signin');

// Add v2 pages that do not requie auth or content_admin middleware 
Route::group(['prefix' => 'v2'], function() {
    // Auth pages
    Route::get('/signup',                           'UsersController@signUp')->name('v2_user_signup');
    Route::get('/signup-corporate',                 'UsersController@signUpCorporate')->name('v2_user_signup_corporate');
    Route::post('/add-new-corporate',               'Dashboard\CorporateController@add_corporate')->name('v2_dashboard_add_corporate');
    
});
