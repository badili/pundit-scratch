@extends('v2.layouts.base_header')

@section('body')
<body>
  @include('partials/v2/header')
<div class="container">
  <!-- Bookmarks -->
  @include('partials/v2/bookmarks')
  <!-- Bookmarks -->
  <!-- breadcrumbs -->
  <ul class="breadcrumbs">
    <li><a href="{{ URL::route('index') }}">Home</a></li>
    <li><a href="{{ URL::route('all_user_acts') }}">Laws and Regulations</a></li>
    <li> {{ ucwords(strtolower($act_version->act()->first()->act_title)) }}, {{ $act_version->act()->first()->cap_no === "" ? "No ". $act_version->act()->first()->cap_no_temp : "Cap No ". $act_version->act()->first()->cap_no }} </li>
  </ul>
  <div class="row">
    <div class="col-md-8">
      <h1 class="treaties-header">The {{ ucwords(strtolower($act_version->act()->first()->act_title)) }} Act <span class='act_year_version'>({{$act_version->version_year}} version)</span></h1>
    </div>
    <div class="col-md-4 text-align-right">
      <div class="row">
        <div class="col-md-8 top-icons">
          <div class="doc-search">
            <input doc_id="{{ $act_version->id }}" id="search_field" class="form-control" name="searchfield" type="text" placeholder='Searches within this document'>
          </div>
        </div>
        <div class="col-md-4 top-icons">
          <span><a href="#" class="view-related-cases"><img src="<?php echo asset('images/court_hammer-icon.svg'); ?>" alt="" title="View Related Cases"></a></span>
          <span><a href="#" class="toggle-acts-sections"><img src="<?php echo asset('images/view-icon.svg'); ?>" alt="" title="Toggle Sections View"></a></span>
          <span><a href="#" title="Report"><img src="<?php echo asset('images/loud_speaker_icon.svg'); ?>" alt=""></a></span>
          <span><a href="#" id="bookmark_add_single_version" user_id="{{Auth::user()->id}}" doc_type="act_version" save_url="{{ url('save_bookmark') }}" doc_id="{{ $act_version->id }}" title="Bookmark this Act"><img src="<?php echo asset('images/bookmark_icon.svg'); ?>" alt=""></a></span>
        </div>
      </div>
          <span>
              
          </span>
    </div>
  </div>
  
  <p class="treaty-signed-info">
    <b>Assent: </b>@if($act_version->assent_date == " " || $act_version->assent_date == "0000-00-00" )
        -- -- ----
    @else
        {{date('jS M Y',strtotime($act_version->assent_date))}}
    @endif	
    <b>Commence:</b>	@if(isset($act_version->commencement_date) && $act_version->commencement_date!=""){{$act_version->commencement_date}}@endif
  </p>
  <div class="treaty-content">
    <!-- related cases -->
  @include('partials/v2/related_cases')
  <!-- related cases -->
    <div id="preamble"></div>
    <!-- start acts sections -->
    <div class="acts-section" id="tabs">
      <!-- categories -->
      <ul class="acts-categories">
        <li role="presentation" class="active"><a data-toggle="tab" href="#sections_content">Sections <span class="count">{{ $total_sections }}</span></a></li>
        <li role="presentation"><a data-toggle="tab" href="#schedules_content">Schedules <span class="count">{{ count($act_version->schedules()->get())  }}</span></a></li>
        <li role="presentation"><a data-toggle="tab" href="#subsidiaries_content" >Subsidiary Legislation <span class="count">{{ count($act_version->subsidiaries()->get()) }}</span></a></li>
      </ul>
 
      <!-- the acts data -->
      <div class="tab-content">
        <div id="sections_content" class="acts-data tab-pane" role="tabpanel">
          @if(count($act_version->parts)>0)
            @foreach($act_version->parts()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->where('doc_type', 'act_part')->get() as $part)
              @if(count($part->sections()->where('act_division_id','=',null)->get())>0)
                <h3>Part {{$part->no}} : {{ $part->title}}</h3>
                <div class="acts-accordion">
                  @foreach($part->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->where('act_division_id','=',null)->orWhere('act_division_id','=',0)->get() as $section)
                    <?php $unique_key = $optimus->encode($section->id); ?>
                    <p class="main-accorion act_section" id="{{ $unique_key }}"  data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                      <div id="collapse_section_{{ $unique_key }}" class='hidden'></div>
                  @endforeach
                </div>
              @endif
              @if(count($part->divisions()->get())>0) 
                <h3>Part {{$part->no}} : {{$part->title}}</h3>
                @foreach($part->divisions()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $division)
                <h5><span>Division {{$division->no}} -</span> <a href="#none" class="tipify" data-original-title="View division in detail">{{$division->title}}</a></h5>
                  <div class="acts-accordion">
                    @foreach($division->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $section)
                      <?php $unique_key = $optimus->encode($section->id); ?>
                      <p class="main-accorion act_section" id="{{ $unique_key }}" data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                        <div id="collapse_section_{{ $unique_key }}" class='hidden' ></div>
                    @endforeach
                  </div>
                @endforeach
              @endif
            @endforeach
          @endif
        </div>
        <div id="schedules_content" class="acts-data accordion tab-pane" role="tabpanel">
          @if(count($act_version->schedules)>0)
            @foreach($act_version->schedules()->get() as $sched)
              <?php $e_schedule_id = $optimus->encode($sched->id); ?>
              <h3><a style="text-decoration: none" href="#">{{$sched->title}}</a></h3> <!-- SHEDULE -->
              <div class="accordion">
                @if(count($sched->parts()->get())>0)
                  @foreach($sched->parts()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $part)
                  <h4><a style="text-decoration: none" href="#">Part {{$part->no}} : {{ $part->title}}</a></h4>
                  <div class="acts-accordion">
                    @foreach($part->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->where('act_division_id','=',null)->orWhere('act_division_id','=',0)->get() as $section)
                      <?php $unique_key = $optimus->encode($section->id); ?>
                      <p style="border: none; border-bottom: 1px solid #e5e5e5;" class="main-accorion act_section" id="{{ $unique_key }}"  data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                        <div id="collapse_section_{{ $unique_key }}" class='hidden'></div>
                    @endforeach
                  </div>
                  @endforeach
                @endif
                @if(count($part->divisions()->get())>0)
                  <h4><a style="text-decoration: none" href="#">Part {{$part->no}} : {{ $part->title}}</a></h4>
                  @foreach($part->divisions()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $division)
                  <h5><span>Division {{$division->no}} -</span> <a href="#none" class="tipify" data-original-title="View division in detail">{{$division->title}}</a></h5>
                    <div class="acts-accordion">
                      @foreach($division->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $section)
                        <?php $unique_key = $optimus->encode($section->id); ?>
                        <p style="border: none; border-bottom: 1px solid #e5e5e5;" class="main-accorion act_section" id="{{ $unique_key }}" data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                          <div id="collapse_section_{{ $unique_key }}" class='hidden'></div>
                      @endforeach
                    </div>
                  @endforeach
                @endif
              </div>
            @endforeach
          @else
            <h3>No Schedules</h3>
          @endif
        </div>
        <div id="subsidiaries_content" class="acts-data accordion tab-pane" role="tabpanel">
          @if(count($act_version->subsidiaries)>0)
            @foreach($act_version->subsidiaries()->get() as $subs)
              <?php $e_subs_id = $optimus->encode($subs->id); ?>
              <h3><a style="text-decoration: none" href="#">{{$subs->title}}</a></h3> <!-- SUBSIDIARY-->
              <div class="accordion">
                @if(count($subs->parts()->get())>0)
                  @foreach($subs->parts()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $part)
                    @if($part->no == '')
                      <h4><a style="text-decoration: none" href="#">{{ $part->title}}</a></h4>
                    @else
                      <h4><a style="text-decoration: none" href="#">Part {{$part->no}} : {{ $part->title}}</a></h4>
                    @endif
                  <div class="acts-accordion">
                    @foreach($part->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->where('act_division_id','=',null)->orWhere('act_division_id','=',0)->get() as $section)
                      <?php $unique_key = $optimus->encode($section->id); ?>
                      <p style="border: none; border-bottom: 1px solid #e5e5e5;" class="main-accorion act_section" id="{{ $unique_key }}"  data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                        <div id="collapse_section_{{ $unique_key }}" class='hidden'></div>
                    @endforeach
                  </div>
                  @endforeach
                @endif
                @if(count($part->divisions()->get())>0)
                  <h4><a style="text-decoration: none" href="#">Part {{$part->no}} : {{ $part->title}}</a></h4>
                  @foreach($part->divisions()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $division)
                  <h5><span>Division {{$division->no}} -</span> <a href="#none" class="tipify" data-original-title="View division in detail">{{$division->title}}</a></h5>
                    <div class="acts-accordion">
                      @foreach($division->sections()->orderBy('rank', 'ASC')->orderBy('id', 'ASC')->get() as $section)
                        <?php $unique_key = $optimus->encode($section->id); ?>
                        <p style="border: none; border-bottom: 1px solid #e5e5e5;" class="main-accorion act_section" id="{{ $unique_key }}" data-section="{{ $unique_key }}"><span><img src="<?php echo asset('images/accorion-arrow.svg'); ?>" alt=""></span> {{(isset($section->no)&&$section->no!='')?$section->no.'.':'?'}} {{(isset($section->title)&&$section->no!='')?$section->title:'Section title goes here'}}</p>
                          <div id="collapse_section_{{ $unique_key }}" class='hidden'></div>
                      @endforeach
                    </div>
                  @endforeach
                @endif
              </div>
            @endforeach
          @endif
        </div>
        </div>
      </div>
    </div>
  </div>
  <div id='loading_overlay' class='hidden'>&nbsp;</div>
</div>
  <!-- start footer -->
   @include('partials/v2/footer')
  <!-- support button -->
  @include('partials/v2/support_button')
  <!-- support button -->
  <div id='act_actions' style="visibility: hidden">
      <dl class="dl-horizontal">
          <dd><pre>%s</pre></dd>
      </dl>
      <ul class="inline unstyled ul_sect_actions">
          <ul class="inline unstyled ul_sect_actions">
              <li><a href="javascript:void(0);" class=" view_section btn btn--primary" data-section="%s" data-original-title="View section in detail" >Jump to section</a></li>
              @if(Request::is('dashboard/*'))
              <li><a href="javascript:void(0);" class="start_content_edit btn btn-small btn-primary tipify act_section" data-section="%s" data-original-title="Edit section" >Edit <i class="fa fa-edit"></i></a></li>
              @endif
          </ul>
      </ul>
  </div>
  <div id="xref_template" style="display: none;">Loading the cross reference...</div>
</body>
@endsection
@section('scripts')
@yield('editor_js')
<script src="<?php echo asset('js/tippy.all.min.js'); ?>"></script>
<script src="<?php echo asset('js/quill.js'); ?>"></script>
@vite(['resources/assets/scripts/badili_editor.js'])
@vite(['resources/assets/scripts/badili_acts.js'])
<script type="module">
  $(function() {
    // Subsidiaries and Schedules accordions
    var badili_common = window.badili_common;
    var badili_acts = window.badili_acts;
    badili_common.initiateBookmarkTabs();
    $( "#tabs" ).tabs({
      collapsible: true,
      activate: function(event, ui) {
        // on activating any tab unhighlight the rest and activate the selected one
          $(this).find('li').removeClass('active');
          $(this).find('li.ui-state-active').addClass('active')
      }
    });
    $("div.accordion").accordion({
        heightStyle: "content",
        collapsible: true,
        active: false,
        navigation: true
    });

    badili_acts.renderPreamble({{ $act_version->id }});
    badili_common.initiateTooltips();
    var save_bookmarks_url = '{!! url('save_bookmark') !!}'

    // Within document search
    badili_acts.cur_version_id = '{!! $optimus->encode($act_version->id) !!}';
    badili_acts.act_version_search = '{!! url('act_version_search') !!}';
    badili_acts.withinDocSearch();
  });
</script>
@endsection
