<?php  
use App\Models\Act, App\Models\ActVersion, App\Models\CourtCase, App\Models\TaxTreaty;
?>
@extends('v2.layouts.base_header')

@section('css')
  <link href="<?php echo asset('css/jquery.fancybox.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo asset('css/fullcalendar.min.css'); ?>" rel="stylesheet">
@endsection
@section('body')
<body>
  @include('partials/v2/header')
  <!-- start image section -->
  <div class="page__hero nairobi-sunset">
    <div class="container title"><h1>News</h1></div>
    <div class="scroll-down">
      <a href="#main"><img src="<?php echo asset('images/down_arrow.svg'); ?>" alt="Down arrow"></a>
    </div>
  </div>
  <!-- start content area -->
  <div class="container news_main" id="main">
      <h3>News</h3>
      <!-- first news item -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <script type="text/javascript"> var news_ids = []; </script>
      @foreach( $news as $related_news_item)
        <div class="col-sm-4">
          <div class="inner">
              <div class="image">
                <img src="{{URL::asset('storage'.'/'.'uploads'.'/'.'news'.'/'.$related_news_item->image_name )}}" alt="News image" onerror="this.src='<?php echo asset('images/default_news.png'); ?>'" width="350" height="170">
               </div>
               <div class="news_intro">
                  <p class="article-dates"><span><img src="<?php echo asset('images/cal_icon.svg'); ?>" alt=""></span>
                    <span>{{\Carbon\Carbon::parse($related_news_item->release_date)->format('d M, Y')}}</span>
                  </p>
                  <h3>{{ $related_news_item->title }}</h3>
                  <?php $o_id = $optimus->encode($related_news_item->id); ?>
                  <p id='o_{{ $o_id }}'>&nbsp;</p>
                  <script type="text/javascript"> news_ids[news_ids.length] = {'ident': 'o_{{ $o_id }}', 'ops': {!! $related_news_item->description !!} };   </script>
                  <p class="learn_more pull-right"><a href="{{route('view_news',['id' => $o_id ])}}" class='red'>Read More</a></p>
               </div>
            </div> 
        </div>
      @endforeach
  </div>
  <div class="container">
      <p class="learn_more text__align__right"><a href="{{route('all-news')}}" class='btn btn--primary'>More</a></p>
  </div>
  <br />

  <!-- Most Popular -->
  <div class="container news_main sys_stats">
    <h2>Most popular:</h2>
    <hr>
    <div class="col-sm-3">
       <div id="news_calendar">
      </div>
      <div class='more_events'>
        <a class="box-icon-more font-lato weight-300" href="{{ URL::route('full-calendar') }}">More Events</a>
      </div>
    </div>
    <div class="col-sm-3">
      <h4>Acts</h4>
      <table class='news_table custom_table'>
        @foreach($final_fav_acts_tally as $act_optimus => $num_visits)
        <?php  
          $id = $optimus->decode($act_optimus);
          $version = ActVersion::where('id', '=', $id)->first();
        ?>
        <tr>
          <td class="long_info laws_table news_table">
            @if(Auth::user())
              <a href="{{ URL::route('user_act_version_display', $act_optimus)}}" class="tipify" title="">
              </a>
            @else
              <a href="{{ URL::route('user_login') }}" class="tipify" title="">
              </a>
            @endif
          </td>
        </tr>
        @endforeach
        </table>
    </div>
    <div class="col-sm-3">
      <h4>Cases</h4>
      <table class='news_table custom_table'>
        @foreach($final_fav_cases_tally as $case_optimus => $num_visits)
        <?php  
          $id = $optimus->decode($case_optimus);
          $case = CourtCase::where('id', $id)->first();
        ?>
        <tr>
          <td class="long_info laws_table news_table">
            @if(Auth::user())
              <a href="{{ $case->is_placeholder == 1 ? '#' : 'case_view/'. $case_optimus }}" id="{{$case->id}}" class="tipify" title="{{ $case->case_narrative }}" data-toggle="modal">
                <i class='fa fa-balance-scale'></i>&nbsp;&nbsp;<span class='title'>{{ $case->plaintiff}} V {{ $case->defendant}}</span> <span class="year">({{\Carbon\Carbon::parse($case->judgement_date)->format('Y')}})</span>
              </a>
            @else
              <a href="{{ URL::route('user_login') }}" class="tipify" title="">
                <i class='fa fa-balance-scale'></i>&nbsp;&nbsp;<span class='title'>{{ $case->plaintiff}} V {{ $case->defendant}}</span> <span class="year">({{\Carbon\Carbon::parse($case->judgement_date)->format('Y')}})</span>
              </a>
            @endif
          </td>
        </tr>
        @endforeach
        </table>
    </div>
    <div class="col-sm-3">
      <h4>Treaties</h4>
      <ul>
        @foreach($addons as $resource)
          <li>
            <?php 
                $download_url = route('download_file',['id' => $resource->id , 'name' => 'resource']);
                $resource_title = $resource->title;
            ?>
            @if(Auth::user())
              @if($resource->file_type == "pdf")
                <a data-fancybox data-type="iframe" data-src="{{$download_url}}" data-caption="Resource Title : {{ $resource_title }}" href="#">{{ $resource_title }}</a>
              @elseif($resource->file_type == "xlsx"))
                <a href="{{$download_url}}" > </a>
              @else
                <a data-fancybox="images" href="{{$download_url}}" data-caption='Resource Title {{ $resource_title }}: <a href={{$download_url}}> Download </a>'>{{ $resource_title }}</a>
              @endif
            @else
              <a href="{{ URL::route('user_login') }}" class="tipify" title="">{{ $resource_title }}</a>
            @endif
          </li>
        @endforeach
        </ul>
    </div>
  </div>
  <br />
  <!-- End of Most Popular -->

  <!-- Recently Added -->
  <div class="container news_main sys_stats">
    <h2>Recently added:</h2>
    <hr>
    <div class="col-sm-3">
      <h4>Acts</h4>
      <table class='news_table custom_table'>
        @foreach($recent_acts as $act)
        <?php  
          $act_optimus = $optimus->encode($act->id);
          $version = ActVersion::where('id', $act->id)->first();
        ?>
          <tr>
            <td class="long_info laws_table news_table">
              @if(Auth::user())
                <a href="{{ URL::route('user_act_version_display', $act_optimus)}}">
                  <i class='fa fa-book'></i>&nbsp;&nbsp;<span class='title'>{{$version->act()->first()->act_title}}</span> <span class='year'>({{$version->version_year}})</span>
                </a>
              @else
                <a href="{{ URL::route('user_login') }}" class="tipify" title="">
                  <i class='fa fa-book'></i>&nbsp;&nbsp;<span>{{$version->act()->first()->act_title}} - {{$version->version_year}}</span>
                </a>
              @endif
            </td>
          </tr>
        @endforeach
        </table>
    </div>
    <div class="col-sm-3">
      <h4>Cases</h4>
      <table class='news_table custom_table'>
        @foreach($recent_cases as $case)
          <?php  
            $case_optimus = $optimus->decode($case->id);
            $case = CourtCase::where('id', $case->id)->first();
          ?>
          <tr>
            <td class="long_info laws_table news_table">
              <p>
                @if(Auth::user())
                <a href="{{ 'case_view/'. $optimus->encode($case->id) }}" id="{{$case->id}}" >
                  <i class='fa fa-balance-scale'></i>&nbsp;&nbsp;<span class='title'>{{ $case->plaintiff}} V {{ $case->defendant}}</span> <span class="year">({{\Carbon\Carbon::parse($case->judgement_date)->format('Y')}})</span>
                </a>
                @else
                  <a href="{{ URL::route('user_login') }}" class="tipify" title="">
                    <i class='fa fa-balance-scale'></i>&nbsp;&nbsp;<span class='title'>{{ $case->plaintiff}} V {{ $case->defendant}}</span> <span class="year">({{\Carbon\Carbon::parse($case->judgement_date)->format('Y')}})</span>
                  </a>
                @endif
              </p>
            </td>
          </tr>
        @endforeach
      </table>
    </div>
    <div class="col-sm-3">
      <h4>Treaties</h4>
      <table class='custom_table news_table'>
        @foreach($recent_treaties as $treaty)
        <?php  
          $treaty_optimus = $optimus->decode($treaty->id);
          $treaty = TaxTreaty::where('id', $treaty->id)->first();

          $versions = $treaty->articles()->first();
          $f_section_id = $optimus->encode($versions->sections()->first()->id);
        ?>
          <tr>
            <td class='long_info laws_table'>
              @if(Auth::user())
                <a href="{{ 'view_treaty_article?section_id='. $f_section_id }}">
                  <span class="title">{{ $treaty->partner2->variable_name }}</span>: {{ $treaty->treaty_name }} ({{ \Carbon\Carbon::parse($treaty->signing_date)->format('Y') }})
                </a>
              @else
                <a href="{{ URL::route('user_login') }}" class="tipify" title="">
                  <span class="title">{{ $treaty->partner2->variable_name }}</span>: {{ $treaty->treaty_name }} ({{ \Carbon\Carbon::parse($treaty->signing_date)->format('Y') }})
                </a>
              @endif
          </td>
          </tr>
        @endforeach
      </table>
    </div>
    <div class="col-sm-3">
      <h4>PwC publications</h4>
        <table class='custom_table news_table'>
          @foreach($recent_publications as $addon)
            <?php $download_url = empty($addon->file_name) ? '#' : route('download_file',['id' => $addon->id , 'name' => 'resource']); ?>
            <tr>
              <td class='long_info laws_table'>
                @if(Auth::user())
                  @if($addon->file_type == "pdf")
                    <a data-fancybox data-type="iframe" data-src="{{$download_url}}" data-caption="Notice Title : {{ $addon->title }}" href="#">
                      <span class="has-media media-pdf title">
                       {{ $addon->title }}
                     </span>
                    </a>
                  @else
                    <a href="{{$download_url}}" data-fancybox="images" data-caption="Notice Title : {{ $addon->title }}">
                      <span class="has-media media-image title">
                        {{ $addon->title }}
                      </span>
                    </a>
                  @endif
                @else
                  <a href="{{ URL::route('user_login') }}" class="tipify" title="" data-caption="Notice Title : {{ $addon->title }}" >
                      <span class="has-media media-pdf title">
                       {{ $addon->title }}
                     </span>
                  </a>
                @endif
              </td> 
            </tr>
          @endforeach
        </table>
    </div>
  </div>
  <!-- End of Recently Added -->

@include('partials/v2/footer')
@include('partials/v2/support_button')
</body>
@endsection

@section('scripts')
<script src="<?php echo asset('js/quill.js'); ?>"></script>
<script src="<?php echo asset('js/jquery.fancybox.min.js'); ?>"></script>
<script src="<?php echo asset('js/moment.min.js'); ?>"></script>
<script src="<?php echo asset('js/fullcalendar.min.js'); ?>"></script>

@vite(['resources/assets/scripts/badili_news.js'])
<script type="module">
  let badili_news = window.badili_news;
  $(document).ready(function () {
    // load the news description snippet
    badili_news.loadNewsSnippets();
    $('#news_calendar').fullCalendar({
      defaultView: 'listWeek',
      height: 350,
      header: {
        left: 'prev',
        center: 'title',
        right: 'next'
      },
      showNonCurrentDates: false,
      eventBorderColor: '#fff',
      eventBackgroundColor: '#e0301e',
      inline: true,
      showOtherMonths: true,
      displayEventTime: false,
      // this code doesn't work. Its meant to show 2 week listing on the home page
      views: {
        week: {
          type: 'week',
          duration: { weeks: 2 }
        }
      },
      // -- end of not working code
      events: {
        url: '/get_events',
        type: 'POST',
        error: function() {
          alert('there was an error while fetching events!');
        },
        success: function(data){
            var all_data = [];
            $.each(data, function(){
                var s_date = new Date(this.start);
                var e_date = new Date(this.start);
                var that = {'start': s_date.toISOString().slice(0, 10), 'end': e_date.toISOString().slice(0, 10), allDay: true};
                // that.backgroundColor = (that.event_owner == 'pwc_event') ? '#e0301e' : '#70141e';
                // that.rendering = 'background';
                all_data[all_data.length] = this;
            });
            // console.log(all_data);
            return all_data;
        }
      },
      eventBackgroundColor: '#e0301e',
      columnHeaderText: function(mom) {
        if (mom.weekday() === 0) return 'S';
        else if (mom.weekday() === 1) return 'M';
        else if (mom.weekday() === 2) return 'T';
        else if (mom.weekday() === 3) return 'W';
        else if (mom.weekday() === 4) return 'T';
        else if (mom.weekday() === 5) return 'F';
        else if (mom.weekday() === 6) return 'S';
      },
      viewRender: function(view, element){
        $('#news_calendar table').addClass('ui-datepicker-calendar');
      }
    });
  });
</script>
@endsection
